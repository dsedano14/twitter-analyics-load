package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TweetUserList implements Serializable {

    @JsonProperty
    private List<TweetUser> tweetUserList= new ArrayList<TweetUser>();



    public List<TweetUser> getTweetUserList() {
        try{
            return tweetUserList;
        }catch(Exception e){
            throw e;
        }
    }


    public void AddUser(TweetUser tweetUser){
        tweetUserList.add(tweetUser);
    }
}
