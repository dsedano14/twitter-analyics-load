package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class EntityList implements Serializable {

    @JsonProperty
    private List<EntityTopic> EntityList= new ArrayList<EntityTopic>();



    public List<EntityTopic> getEntityList() {
        try{
            return EntityList;
        }catch(Exception e){
            throw e;
        }
    }


    public void AddEntity(EntityTopic entity){
        EntityList.add(entity);
    }
}
