package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TweetTrendList implements Serializable{

    @JsonProperty
    private List<TweetTrend> tweetTrendList= new ArrayList<TweetTrend>();



    public List<TweetTrend> getTweetTrendList() {
        try{
            return tweetTrendList;
        }catch(Exception e){
            throw e;
        }
    }


    public void AddTrend(TweetTrend tweetTrend){
        tweetTrendList.add(tweetTrend);
    }
}
