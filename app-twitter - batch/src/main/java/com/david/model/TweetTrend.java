package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TweetTrend implements Serializable{
    @JsonProperty
    private String name;
    @JsonProperty
    private String query;
    @JsonProperty
    private Integer tweetVolume;
    @JsonProperty
    private String url;
    @JsonProperty
    private String place;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public Integer getTweetVolume() {
        return tweetVolume;
    }

    public void setTweetVolume(Integer tweetVolume) {
        this.tweetVolume = tweetVolume;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
