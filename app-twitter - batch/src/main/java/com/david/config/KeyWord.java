package com.david.config;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class KeyWord  implements Serializable{

    @JsonProperty
    private String name;
    @JsonProperty
    private String type;

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

}
