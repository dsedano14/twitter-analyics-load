package com.david.publisher;

import com.david.config.keywordConfig;
import com.david.model.TweetTrend;
import com.david.model.TweetTrendList;
import twitter4j.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static com.david.publisher.TwitterUser.GetKeyWordsYAML;

public class TwitterTrends {

    public static String[] GetTrendPlaces(){

        keywordConfig entitiesFromYAML = GetKeyWordsYAML();

        List<String> key= new ArrayList<>();

        entitiesFromYAML.getKeyWord().stream()
                .filter(s->s.getType().contentEquals("trendPlace"))
                .forEach(a -> key.add(a.getName()));

        String keywords[]=new String[key.size()];
        System.out.println(key.size());

        for (int i =0; i < key.size(); i++){
            keywords[i] = key.get(i);
            System.out.println(keywords);
        }

        return keywords;
    }



    public static HashMap<String, Integer> GetTwitterLocationFromYAML(String[] locationsYAML, ResponseList<Location> locationsTwitter){

        HashMap<String, Integer> LocationsFound = new HashMap<String, Integer>();

        for (String locationCustom : locationsYAML) {
            for (Location locationTwit : locationsTwitter) {
                if(locationCustom.equals(locationTwit.getName())){
                    LocationsFound.put(locationTwit.getName(), locationTwit.getWoeid());
                }
            }
        }

        return LocationsFound;
    }



    public static HashMap<String, Trends> GetTrendFromWoied(HashMap<String, Integer> locations, Twitter twitter){

            HashMap<String, Trends> trendsHashMap= new HashMap<>();
            ArrayList<Trends> trends = new ArrayList<>();


            locations.forEach((k,v) -> {
                try {
                    trendsHashMap.put(k,twitter.getPlaceTrends(v));
                    trends.add(twitter.getPlaceTrends(v));
                } catch (TwitterException e) {
                    e.printStackTrace();
                }
            });

        return trendsHashMap;
    }

    public static TweetTrendList GetTwitterTrends(Twitter twitter){

        TweetTrendList trendList = new TweetTrendList();

        ResponseList<Location> locations;

        try {
            locations = twitter.getAvailableTrends();

            String LocationYAML[] = GetTrendPlaces();
            HashMap<String, Integer> LocationsFound = GetTwitterLocationFromYAML(LocationYAML, locations);

            System.out.println("Showing trends");
            System.out.println(LocationsFound.toString());

            //ArrayList<Trends> placeTrends = GetTrendFromWoied(LocationsFound, twitter);
            HashMap<String, Trends> placeTrends = GetTrendFromWoied(LocationsFound, twitter);

            placeTrends.forEach((k,v) -> {
                for (Trend trend : v.getTrends()) {
                    TweetTrend trendInput = new TweetTrend();
                    trendInput.setName(trend.getName());
                    trendInput.setQuery(trend.getQuery());
                    trendInput.setTweetVolume(trend.getTweetVolume());
                    trendInput.setUrl(trend.getURL());
                    trendInput.setPlace(k);
                    trendList.AddTrend(trendInput);//string
                }
            });


        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to get trends: " + te.getMessage());
        }

        return trendList;
    }

}
