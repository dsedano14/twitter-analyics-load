package com.david.publisher;

import com.david.config.keywordConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import twitter4j.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.david.utils.Utils.checkIfExistsQuoted;


public class TwitterUser {

    public static keywordConfig GetKeyWordsYAML(){

        Storage storage = StorageOptions.getDefaultInstance().getService();
        keywordConfig entitiesFromYAML = null;
        Blob blob = storage.get(BlobId.of("app-twitter-temp", "temp-loc/keywords.yaml"));

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            entitiesFromYAML = mapper.readValue(blob.getContent(), keywordConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entitiesFromYAML;
    }

    public static String[] GetUsers(){

        keywordConfig entitiesFromYAML = GetKeyWordsYAML();

        List<String> key= new ArrayList<>();

        entitiesFromYAML.getKeyWord().stream()
                .filter(s->s.getType().contentEquals("user"))
                .forEach(a -> key.add(a.getName()));

        String keywords[]=new String[key.size()];
        System.out.println(key.size());

        for (int i =0; i < key.size(); i++){
            keywords[i] = key.get(i);
            System.out.println(keywords);
        }

        return keywords;
    }

    public static List<User> GetTwitterUsersFromYAML(String[] usersYAML, Twitter twitter){

        List<User> usersFound = new ArrayList<>();

        for (String usersCustom : usersYAML) {
            try {
                usersFound.add(twitter.showUser(usersCustom));

            } catch (TwitterException e) {
                e.printStackTrace();
            }
        }

        return usersFound;
    }



    public static List<User> GetTwitterUsers(Twitter twitter) {

        List<User> userList = new ArrayList<>();

        String usersYAML[] = GetUsers();


        userList = GetTwitterUsersFromYAML(usersYAML, twitter);


        return userList;
    }

}
