﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics
{
    class UserStatus
    {
        public string name { get; set; }
        public string text { get; set; }
        public string created_datetime { get; set; }
        public string retweet_count { get; set; }
        public string fav_count { get; set; }
        public string status_link { get; set; }

        public UserStatus()
        {
        }
    }
}
