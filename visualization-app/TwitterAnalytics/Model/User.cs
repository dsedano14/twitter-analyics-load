﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class User
    {
        public List<UserEvolution> user_evolution { get; set; }
        public List<UserStatus> user_status { get; set; }

        public User()
        {
        }
    }
}
