﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class HashtagEvolution
    {
        public string name { get; set; }
        public string time { get; set; }
        public string num_status { get; set; }
        public string num_users { get; set; }
        public string num_retweets { get; set; }
        public string num_favorite { get; set; }
        public string total_status { get; set; }
        public string total_users { get; set; }
        public string total_retweets { get; set; }
        public string total_favorite { get; set; }

        public HashtagEvolution() { }
    }
}
