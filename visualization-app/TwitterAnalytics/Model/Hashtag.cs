﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class Hashtag
    {
        //uso dictionary para pasar el nombre que aparecerá en el selector y la lista de resultados con los que trabajar
        public Dictionary<string, List<HashtagEvolution>> hashtag_evolution { get; set; }
        //public List<HashtagEvolution> hashtag_evolution { get; set; }
        public List<HashtagLastStatus> last_status { get; set; }

        public Hashtag()
        {
        }
    }
}
