﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class TrendTop
    {
        public String day { get; set; }
        public String place { get; set; }
        public String name { get; set; }
        public String query { get; set; }
        public String tweet_volume { get; set; }

        public TrendTop()
        {
        }
    }
}
