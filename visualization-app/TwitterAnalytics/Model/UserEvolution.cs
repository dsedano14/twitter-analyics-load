﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics
{
    class UserEvolution
    {
        public string name { get; set; }
        public string screenname { get; set; }
        public string desc { get; set; }
        public string url { get; set; }
        public string location { get; set; }
        public string status_count { get; set; }
        public string img_url { get; set; }
        public string[] follower { get; set; }
        public string[] friend { get; set; }
        public string[] favorite { get; set; }
        public string[] date { get; set; }

        public UserEvolution() {

            
        }
    }
}
