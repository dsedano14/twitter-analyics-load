﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class Trend
    {
        public List<TrendTop> trends_array { get; set; }
        public List<TrendRepeated> trends_repeated { get; set; }

        public Trend()
        {
        }
    }
}
