﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class HashtagLastStatus
    {
        public string name { get; set; }
        public string creation_datetime { get; set; }
        public string text { get; set; }
        public string fav_count { get; set; }
        public string retweet_count { get; set; }
        public string user_location { get; set; }
        public string link { get; set; }

        public HashtagLastStatus() { }
    }
}
