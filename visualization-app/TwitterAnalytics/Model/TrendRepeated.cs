﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics.Model
{
    class TrendRepeated
    {
        public String trend_place { get; set; }
        public String trend_name { get; set; }
        public String num_repeated { get; set; }
  
        public TrendRepeated()
        {
        }
    }
}
