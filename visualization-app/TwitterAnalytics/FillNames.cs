﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAnalytics
{
    class FillNames
    {
        /*public static string user_evolution_query = "SELECT b.user_name, b.user_screenname, b.user_desc, b.user_url, b.user_location, b.user_status_count, b.user_profile_img_url as img_url, ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_follower_count as STRING) order by a.inserted_date asc), ', ') as follower "
            + " , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_favorite_count as STRING) order by a.inserted_date asc), ', ') as favorite"
            + " , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_friends_count as STRING) order by a.inserted_date asc), ', ') as friend"
            + " , ARRAY_TO_STRING(ARRAY_AGG(CAST(TIMESTAMP_TRUNC(a.inserted_date, HOUR) as STRING) order by a.inserted_date asc), ', ') as date  FROM `dsedano-datalake-twitter.ocean.user_progress` a"
            + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id = b.user_id"
            + " group by 1,2,3,4,5,6,7"
            + " order by 1 desc";*/

        public static string user_evolution_query = "SELECT b.user_name, b.user_screenname, b.user_desc, b.user_url, b.user_location, b.user_status_count, b.user_profile_img_url as img_url, ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_follower_count as STRING) order by a.day asc), ', ') as follower"
            + " , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_favorite_count as STRING) order by a.day asc), ', ') as favorite"
            +" , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_friends_count as STRING) order by a.day asc), ', ') as friend"
            +" , ARRAY_TO_STRING(ARRAY_AGG(CONCAT( LPAD(CAST(EXTRACT(DAY from a.day) as STRING),2,'0'),'/', LPAD(CAST(EXTRACT(MONTH from a.day) as STRING),2,'0'))  order by a.day asc), ', ') as date  "
            +" FROM (SELECT user_id, day, max(user_friends_count) user_friends_count, max(user_favorite_count) user_favorite_count, max(user_follower_count) user_follower_count"
            +" FROM `dsedano-datalake-twitter.dwh.user_progress` a"
            +" where a.day>= DATE_SUB(current_date, INTERVAL 30 DAY)"
            +" group by 1,2) a"
            +" JOIN `dsedano-datalake-twitter.dwh.user` b on a.user_id = b.user_id"
            +" group by 1,2,3,4,5,6,7";

        /*public static string user_last_status_query = "SELECT distinct a.user_name, case when b.status_quoted='Not Quoted' THEN case when status_retweeted_text = 'Not Retweeted' then b.status_text_value else status_retweeted_text end else status_quoted end as text"
            + " , status_creation_datetime, status_retweet_count, IFNULL(favorite_count,0) as favorite_count, concat('https://twitter.com/', user_screenname,'/status/', cast(status_id as string)) as status_link FROM `dsedano-datalake-twitter.raw_last.user_twitter` a"
            + " join `dsedano-datalake-twitter.raw_last.status_twitter` b on a.user_id=b.user_id"
            + " where a.user_orig='user'";*/

        public static string user_last_status_query = "SELECT * EXCEPT(row_number) FROM("
            + " SELECT distinct a.user_name, case when b.status_quoted='Not Quoted' THEN case when status_retweeted_text = 'Not Retweeted' then b.status_text_value else status_retweeted_text end else status_quoted end as text"
            +" , status_creation_datetime, status_retweet_count, IFNULL(favorite_count,0) as favorite_count, concat('https://twitter.com/', user_screenname,'/status/', cast(status_id as string)) as status_link"
            +" , ROW_NUMBER() OVER(PARTITION BY a.user_id ORDER BY b.status_creation_datetime desc,b.day desc) row_number"
            +" FROM `dsedano-datalake-twitter.dwh.user` a"
            +" join `dsedano-datalake-twitter.dwh.twitter_tweets` b on a.user_id=b.user_id"
            +" where a.user_orig='user'"
            +" )c"
            +" where row_number<10" +
            " order by status_creation_datetime desc";

        public static string trend_top_10_query = "SELECT day, trend_place, trend_name, CONCAT('https://twitter.com/search?q=',trend_query,'&src=typd&lang=es') as query, trend_tweet_volume FROM `dsedano-datalake-twitter.dwh.daily_trend_twitter_top10`"
            + " where day>=DATE_SUB(current_date, INTERVAL 30 Day) order by 1 desc, 2 asc, 5 desc";

        public static string trend_top_repeated_month = "SELECT * EXCEPT(row_number) FROM ("
            + " SELECT trend_place, trend_name, count(*) as num_repeated, ROW_NUMBER() OVER(PARTITION BY trend_place ORDER BY count(*) desc) row_number FROM `dsedano-datalake-twitter.dwh.trending_topic_daily`"
            + " where day>=DATE_SUB(current_date, INTERVAL 30 DAY)"
            + " group by 1,2"
            + " order by 1)c"
            + " where row_number<=10";

        public static string hashtag_last_status = "SELECT * EXCEPT(row_number) FROM(SELECT c.hashtag_name, a.status_creation_datetime, case when a.status_quoted= 'Not Quoted' THEN case when status_retweeted_text = 'Not Retweeted' then a.status_text_value else status_retweeted_text end else status_quoted end as text"
            +" , favorite_count, status_retweet_count, b.user_location, concat('https://twitter.com/', user_screenname,'/status/', cast(a.status_id as string)) as status_link, ROW_NUMBER() OVER(PARTITION BY hashtag_name ORDER BY status_creation_datetime desc) row_number"
            +" FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
            +" JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id=b.user_id"
            +" JOIN `dsedano-datalake-twitter.raw.topic_twitter` c on c.status_id=a.status_id" 
            +" where b.user_orig='status'"
            + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 1 DAY)"
            + " and c.hashtag_name<>'null'"
            +" order by 1,2 desc)c where row_number<=10";

        public static string hashtag_popular_status = "SELECT* EXCEPT(row_number) FROM(SELECT  case when c.hashtag_name in ('barcelona','bcn') then 'barcelona' when c.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
        + " when c.hashtag_name in ('europe','europa') then 'europa' else c.hashtag_name end as hashtag_name, a.status_creation_datetime, stat.num_retweets , case when a.status_quoted= 'Not Quoted' THEN case when status_retweeted_text = 'Not Retweeted' then a.status_text_value else status_retweeted_text end else status_quoted end as text"
        + " , favorite_count, status_retweet_count, b.user_location, concat('https://twitter.com/', user_screenname,'/status/', cast(a.status_id as string)) as status_link, ROW_NUMBER() OVER(PARTITION BY  case when c.hashtag_name in ('barcelona','bcn') then 'barcelona' when c.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
        + " when c.hashtag_name in ('europe','europa') then 'europa' else c.hashtag_name end ORDER BY stat.num_retweets desc) row_number"
        + " FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
        + " JOIN(SELECT status_retweeted_status_id, count(*) as num_retweets"
        + " FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
        + " where a.status_retweeted_status_id<> 0"
        + " group by 1"
        + " order by 2 desc)stat on stat.status_retweeted_status_id=a.status_id"
        + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id=b.user_id"
        + " JOIN `dsedano-datalake-twitter.raw.topic_twitter` c on c.status_id=a.status_id"
        + " where b.user_orig='status'"
        + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 30 DAY)"
        + " and c.hashtag_name<>'null'"
        + " order by 1,3 desc)c where row_number<=10";

      /*  public static string hashtag_evolution_last_hour = "SELECT *, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets from ( "
            + " SELECT case when c.hashtag_name in ('barcelona','bcn') then 'barcelona' when c.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when c.hashtag_name in ('europe','europa') then 'europa' else c.hashtag_name end as hashtag_name"
            + " ,CONCAT(SUBSTR(CAST(EXTRACT(TIME from a.status_creation_datetime) AS STRING),0,4), '0') as time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users, count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
            + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id=b.user_id"
            + " JOIN `dsedano-datalake-twitter.raw.topic_twitter` c on c.status_id=a.status_id"
            + " where b.user_orig='status'"
            + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 60 MINUTE)"
            + " and c.hashtag_name<>'null'"
            + " group by 1,2"
            + " order by 1,2)c;";*/
            
        public static string hashtag_evolution_last_hour = "SELECT *, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets  " +
            ", SUM(favorite) OVER (PARTITION BY c.hashtag_name) AS total_fav from ( "
            +" SELECT hashtag_name"
            +" ,  time"
            +" , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users"
            +" , sum(a.status_retweet_count) + count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            +" , sum(a.favorite_count) as favorite"
            +" FROM (SELECT a.status_id, a.user_id, status_retweeted_status_id, status_retweet_count, favorite_count, case when b.hashtag_name in ('barcelona','bcn') then 'barcelona' when b.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            +" when b.hashtag_name in ('europe','europa') then 'europa' else b.hashtag_name end as hashtag_name"
            +" ,CONCAT(SUBSTR(CAST(EXTRACT(TIME from a.status_creation_datetime) AS STRING),0,4), '0') as time"
            +" FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
            +" join  `dsedano-datalake-twitter.raw_last.topic_twitter` b on a.status_id=b.status_id"
            +"  and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(), INTERVAL 60 MINUTE)"
            +" and b.hashtag_name<>'null' )a"
            +" group by 1,2"
            +" order by 1,2)c";

     /*   public static string hashtag_evolution_last_day = "SELECT *, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets from ( "
            + " SELECT case when a.hashtag_name in ('barcelona','bcn') then 'barcelona' when a.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when a.hashtag_name in ('europe','europa') then 'europa' else a.hashtag_name end as hashtag_name"
            + " , EXTRACT(HOUR from a.status_creation_datetime) as time"
            //+ " , TIMESTAMP(CONCAT(SUBSTR(CAST(a.status_creation_datetime as STRING),0,14), '00:00')) as time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users, count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " FROM `dsedano-datalake-twitter.dwh.twitter_tweets` a"
            + " JOIN `dsedano-datalake-twitter.dwh.user` b on a.user_id=b.user_id"
            + " where b.user_orig='status'"
            + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 1 DAY)"
            + " and a.hashtag_name<>'null'"
            + " group by 1,2"
            + " order by 1,2)c;";*/

        public static string hashtag_evolution_last_day = "SELECT *, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets  , SUM(favorite) OVER" + " (PARTITION BY c.hashtag_name) AS total_fav from ( "
            + " SELECT hashtag_name"
            + " ,  time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users"
            + " , sum(a.status_retweet_count) + count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " , sum(a.favorite_count) as favorite"
            + " FROM (SELECT status_id, a.user_id, status_retweeted_status_id, status_retweet_count, favorite_count, case when a.hashtag_name in ('barcelona','bcn') then 'barcelona' when a.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when a.hashtag_name in ('europe','europa') then 'europa' else a.hashtag_name end as hashtag_name"
            + " , EXTRACT(HOUR from a.status_creation_datetime) as time"
            + " FROM `dsedano-datalake-twitter.dwh.twitter_tweets` a"
            + " where  a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 1 DAY)"
            + " and a.hashtag_name<>'null') a"
            + " group by 1,2"
            + " order by 1,2)c;";

        /*public static string hashtag_evolution_last_week = "SELECT hashtag_name, time, num_status, num_users, retweets, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets from ( "
            + " SELECT case when c.hashtag_name in ('barcelona','bcn') then 'barcelona' when c.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when c.hashtag_name in ('europe','europa') then 'europa' else c.hashtag_name end as hashtag_name"
            + " , EXTRACT(DATE from a.status_creation_datetime) as date"
            + " , EXTRACT(DAY from a.status_creation_datetime) as time"
            //+ " , TIMESTAMP(CONCAT(SUBSTR(CAST(a.status_creation_datetime as STRING),0,14), '00:00')) as time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users, count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
            + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id=b.user_id"
            + " JOIN `dsedano-datalake-twitter.raw.topic_twitter` c on c.status_id=a.status_id"
            + " where b.user_orig='status'"
            + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 7 DAY)"
            + " and c.hashtag_name<>'null'"
            + " group by 1,2,3"
            + " order by 1,2 asc)c;";*/

        public static string hashtag_evolution_last_week = "SELECT *, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets  , SUM(favorite) OVER (PARTITION BY c.hashtag_name) AS total_fav from( "
            +" SELECT hashtag_name"
            +" ,  time"
            +" , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users"
            +" , sum(a.status_retweet_count) + count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            +" , sum(a.favorite_count) as favorite"
            +" FROM (SELECT status_id, a.user_id, status_retweeted_status_id, status_retweet_count, favorite_count, case when a.hashtag_name in ('barcelona','bcn') then 'barcelona' when a.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            +" when a.hashtag_name in ('europe','europa') then 'europa' else a.hashtag_name end as hashtag_name"
            +" , EXTRACT(DATE from a.status_creation_datetime) as date"
            +" , EXTRACT(DAY from a.status_creation_datetime) as time"
            +" FROM `dsedano-datalake-twitter.dwh.twitter_tweets` a"
            +" where a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 7 DAY)"
            +" and a.hashtag_name<>'null') a"
            +" group by 1,2"
            +" order by 1,2)c;";

    /*    public static string hashtag_evolution_last_month = "SELECT hashtag_name, time, num_status, num_users, retweets, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets from ( "
            + " SELECT case when c.hashtag_name in ('barcelona','bcn') then 'barcelona' when c.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when c.hashtag_name in ('europe','europa') then 'europa' else c.hashtag_name end as hashtag_name"
            + " , EXTRACT(DATE from a.status_creation_datetime) as date"
            + " , EXTRACT(DAY from a.status_creation_datetime) as time"
            //+ " , TIMESTAMP(CONCAT(SUBSTR(CAST(a.status_creation_datetime as STRING),0,14), '00:00')) as time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users, count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " FROM `dsedano-datalake-twitter.raw_last.status_twitter` a"
            + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id=b.user_id"
            + " JOIN `dsedano-datalake-twitter.raw.topic_twitter` c on c.status_id=a.status_id"
            + " where b.user_orig='status'"
            + " and a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 30 DAY)"
            + " and c.hashtag_name<>'null'"
            + " group by 1,2,3"
            + " order by 1,2 asc)c;";*/

        public static string hashtag_evolution_last_month = "SELECT hashtag_name, time, num_status, num_users, retweets, favorite, SUM(num_status) OVER (PARTITION BY c.hashtag_name) AS total_status , SUM(num_users) OVER (PARTITION BY c.hashtag_name) AS total_users , SUM(retweets) OVER (PARTITION BY c.hashtag_name) AS total_retweets  , SUM(favorite) OVER (PARTITION BY c.hashtag_name) AS total_fav from ( "
            + " SELECT hashtag_name"
            + " , date,  time"
            + " , count(distinct a.status_id) as num_status, count(distinct a.user_id) as num_users"
            + " , sum(a.status_retweet_count) + count(case when a.status_retweeted_status_id= 0 then null else 1 end)  as retweets"
            + " , sum(a.favorite_count) as favorite"
            + " FROM (SELECT status_id, a.user_id, status_retweeted_status_id, status_retweet_count, favorite_count, case when a.hashtag_name in ('barcelona','bcn') then 'barcelona' when a.hashtag_name in ('catalunya', 'cataluña') then 'catalunya'"
            + " when a.hashtag_name in ('europe','europa') then 'europa' else a.hashtag_name end as hashtag_name"
            + "  , EXTRACT(DATE from a.status_creation_datetime) as date"
            + "  , EXTRACT(DAY from a.status_creation_datetime) as time"
            + " FROM `dsedano-datalake-twitter.dwh.twitter_tweets` a"
            + "  where a.status_creation_datetime >= TIMESTAMP_SUB(CURRENT_TIMESTAMP(),INTERVAL 30 DAY)"
            + " and a.hashtag_name<>'null') a"
            + " group by 1,2,3"
            + " order by 1,2 asc)c;";

        public FillNames()
        {

        }
    }
}
