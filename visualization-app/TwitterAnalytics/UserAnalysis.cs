﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts; //Core of the library
using LiveCharts.Wpf; //The WPF controls
using LiveCharts.WinForms; //the WinForm wrappers
using System.Windows.Media;
using Brushes = System.Windows.Media.Brushes;
using TwitterAnalytics.Model;

namespace TwitterAnalytics
{
    public partial class UserAnalysis : Form
    {
        private fMain gg;
        List<UserEvolution> result = new List<UserEvolution>();
        User resultado = new User();

        public UserAnalysis(TwitterAnalytics.fMain p, object user)
        {
            InitializeComponent();
            this.Owner = p;
            gg = p;

            resultado = (User)user;
            //result = (List<UserEvolution>)user;

            result = resultado.user_evolution;

            FillComboBox(result);

            FillLabels(result[0]);
            FollowerChart(result[0]);
            FavoriteChart(result[0]); 
            FriendChart(result[0]);

            FillLastStatus(resultado.user_status);

            System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml("#FFCC66");
            System.Drawing.Color newColor = System.Drawing.Color.FromArgb(125, 51, 157, 243);
            comboBoxUser.Text = result[0].name;

        }

        private void FollowerChart(UserEvolution result)
        {
            cartesianChartFollower.Series.Clear();
            cartesianChartFollower.AxisX.Clear();
            cartesianChartFollower.AxisY.Clear();

            cartesianChartFollower.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Followers",
                    Values = ArrayToChartValue(result.follower)
                }
            };

            cartesianChartFollower.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = result.date
            });

            cartesianChartFollower.AxisY.Add(new Axis
            {
                Title = "Nº Followers"
            });

            cartesianChartFollower.LegendLocation = LegendLocation.None;


            cartesianChartFollower.DataClick += CartesianChart1OnDataClick;
        }

        private void FriendChart(UserEvolution result)
        {
            cartesianChartFriend.Series.Clear();
            cartesianChartFriend.AxisX.Clear();
            cartesianChartFriend.AxisY.Clear();

            cartesianChartFriend.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Friends",
                    Values = ArrayToChartValue(result.friend)
                }
            };

            cartesianChartFriend.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = result.date
            });

            cartesianChartFriend.AxisY.Add(new Axis
            {
                Title = "Nº Friends"
            });

            cartesianChartFriend.LegendLocation = LegendLocation.None;


            cartesianChartFriend.DataClick += CartesianChart1OnDataClick;
        }

        private void FavoriteChart(UserEvolution result)
        {
            cartesianChartFavorite.Series.Clear();
            cartesianChartFavorite.AxisX.Clear();
            cartesianChartFavorite.AxisY.Clear();

            cartesianChartFavorite.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Favorite",
                    Values = ArrayToChartValue(result.favorite)
                }
            };

            cartesianChartFavorite.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = result.date
            });

            cartesianChartFavorite.AxisY.Add(new Axis
            {
                Title = "Nº Favorite"
            });

            cartesianChartFavorite.LegendLocation = LegendLocation.None;


            cartesianChartFavorite.DataClick += CartesianChart1OnDataClick;
        }

        private void CartesianChart1OnDataClick(object sender, ChartPoint chartPoint)
        {
            MessageBox.Show("You clicked (" + chartPoint.X + "," + chartPoint.Y + ")");
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void FillComboBox(List<UserEvolution> entryList)
        {
            foreach(UserEvolution ue in entryList)
            {
                comboBoxUser.Items.Add(ue.name);
            }
        }

        private ChartValues<double> ArrayToChartValue(String[] arrayToTransform)
        {
            ChartValues<double> final_array = new ChartValues<double>();

            for(int u =0; u<arrayToTransform.Count(); u++)
            {

                final_array.Add(Convert.ToDouble(arrayToTransform[u]));
            }

            return final_array;

        }

        private void ComboBoxUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            FollowerChart(result[comboBoxUser.SelectedIndex]);
            FriendChart(result[comboBoxUser.SelectedIndex]);
            FavoriteChart(result[comboBoxUser.SelectedIndex]);
            FillLabels(result[comboBoxUser.SelectedIndex]);
            FillLastStatus(resultado.user_status);

        }

        private void FillLabels(UserEvolution ue)
        {
            labelName.Text = ue.name;
            labelDesc.Text = ue.desc;
            labelLocation.Text = ue.location;
            linkLabelurl.Text = "visit page";
            //linkLabelurl.Links.Add(24, 9, ue.url);
            pictureBoxPhoto.Load(ue.img_url);


            //labelFollower.Text = ue.follower[ue.follower.Count()-1];
            labelFollower.Text = FormatNumber(ue.follower[ue.follower.Count()-1]);
            // labelFriend.Text = ue.friend[ue.follower.Count() - 1];
            labelFriend.Text = FormatNumber(ue.friend[ue.follower.Count() - 1]);
            //labelFavorite.Text = ue.favorite[ue.follower.Count() - 1];
            labelFavorite.Text = FormatNumber(ue.favorite[ue.follower.Count() - 1]);
            labelTweets.Text = FormatNumber(ue.status_count);
            linkLabelurl.Text = ue.screenname;
        }

        private string FormatNumber(string number)
        {
            string output;
            if (Int64.Parse(number) > 1000000)
                output = ((float)(Int64.Parse(number)) / 1000000).ToString("0.00") + "M";
            else if (Int64.Parse(number) > 1000)
                output = ((float)(Int64.Parse(number)) / 1000).ToString("0.00") + "k";
            else
                output = number;

            return output;
        }

        private void ImgReturn_Click(object sender, EventArgs e)
        {
            gg.Show(); 
            this.Close();
        }

        private void ImgMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ImgClose_Click(object sender, EventArgs e)
        {
            gg.Show();
            this.Close();
        }

        private void FillLastStatus(List<UserStatus> statusList)
        {
            dataGridViewLastStatus.Rows.Clear();
            //dataGridViewLastStatus.Refresh();
            //dataGridViewLastStatus.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridViewLastStatus.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewLastStatus.RowHeadersWidth = 20;
            dataGridViewLastStatus.EnableHeadersVisualStyles = false;


            //dataGridViewLastStatus.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(155, 210, 255);

            dataGridViewLastStatus.RowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(218, 238, 255);
            dataGridViewLastStatus.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.White;


            List<UserStatus> filteredstatusList = statusList.Where(x => x.name.Equals(labelName.Text)).ToList();

            foreach (UserStatus ue in filteredstatusList)
            {
                int n = dataGridViewLastStatus.Rows.Add();

                dataGridViewLastStatus.Rows[n].Cells[4].Value = ue.text;
                dataGridViewLastStatus.Rows[n].Cells[0].Value = ue.created_datetime;
                dataGridViewLastStatus.Rows[n].Cells[1].Value = ue.retweet_count;
                dataGridViewLastStatus.Rows[n].Cells[2].Value = ue.fav_count;
                dataGridViewLastStatus.Rows[n].Cells[3].Value = ue.status_link;
            }
            //dataGridViewLastStatus.ClearSelection();
                       
        }

        private void DataGridViewLastStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewLastStatus.SelectedCells[0].ColumnIndex.Equals(3))
            {
                System.Diagnostics.Process.Start(dataGridViewLastStatus.SelectedCells[0].Value.ToString());
            }
            // Get the url from the row
           // var url = e.Row.Columns[1].Value;
           //     Process.Start(url);
            
        }

        private void LinkLabelurl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Console.WriteLine("sadsasddsd");
            //Console.WriteLine((comboBoxUser.Text));

            List<UserEvolution> filteredstatusList = result.Where(x => x.name.Equals(comboBoxUser.Text)).ToList();

            System.Diagnostics.Process.Start("www.twitter.com/"+ filteredstatusList[0].screenname);
        }
    }
}
