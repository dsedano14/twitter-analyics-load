﻿namespace TwitterAnalytics
{
    partial class fHashtag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fHashtag));
            this.labelHeader = new System.Windows.Forms.Label();
            this.comboBoxHashtag = new System.Windows.Forms.ComboBox();
            this.cartesianChartStatus = new LiveCharts.WinForms.CartesianChart();
            this.cartesianChartUser = new LiveCharts.WinForms.CartesianChart();
            this.cartesianChartRetweet = new LiveCharts.WinForms.CartesianChart();
            this.panelUserTotal = new System.Windows.Forms.Panel();
            this.labelUsersResult = new System.Windows.Forms.Label();
            this.labelUserTotal = new System.Windows.Forms.Label();
            this.panelRetweetTotal = new System.Windows.Forms.Panel();
            this.labelRetweetsResult = new System.Windows.Forms.Label();
            this.labelRetweetTotal = new System.Windows.Forms.Label();
            this.panelStatusTotal = new System.Windows.Forms.Panel();
            this.labelStatusResult = new System.Windows.Forms.Label();
            this.labelStatusTotal = new System.Windows.Forms.Label();
            this.panelFavTotal = new System.Windows.Forms.Panel();
            this.labelFavResult = new System.Windows.Forms.Label();
            this.labelFavTotal = new System.Windows.Forms.Label();
            this.labelChartStatus = new System.Windows.Forms.Label();
            this.labelChartUser = new System.Windows.Forms.Label();
            this.labelChartRetweet = new System.Windows.Forms.Label();
            this.labelKPItit = new System.Windows.Forms.Label();
            this.panelHeader = new System.Windows.Forms.Panel();
            this.dataGridViewLastStatus = new System.Windows.Forms.DataGridView();
            this.labelComboHashtag = new System.Windows.Forms.Label();
            this.labelComboTime = new System.Windows.Forms.Label();
            this.comboBoxTimeFrame = new System.Windows.Forms.ComboBox();
            this.labelHashtag = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgClose = new System.Windows.Forms.PictureBox();
            this.imgMin = new System.Windows.Forms.PictureBox();
            this.imgReturn = new System.Windows.Forms.PictureBox();
            this.pictureBoxHeaderBird = new System.Windows.Forms.PictureBox();
            this.pictureBoxFav = new System.Windows.Forms.PictureBox();
            this.pictureBoxTweets = new System.Windows.Forms.PictureBox();
            this.pictureBoxRetweets = new System.Windows.Forms.PictureBox();
            this.pictureBoxUser = new System.Windows.Forms.PictureBox();
            this.createddate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Location = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tweet_link = new System.Windows.Forms.DataGridViewLinkColumn();
            this.tweet_text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelUserTotal.SuspendLayout();
            this.panelRetweetTotal.SuspendLayout();
            this.panelStatusTotal.SuspendLayout();
            this.panelFavTotal.SuspendLayout();
            this.panelHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLastStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeaderBird)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTweets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRetweets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUser)).BeginInit();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.AutoSize = true;
            this.labelHeader.Font = new System.Drawing.Font("Calibri", 12F);
            this.labelHeader.ForeColor = System.Drawing.Color.White;
            this.labelHeader.Location = new System.Drawing.Point(73, 20);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(121, 19);
            this.labelHeader.TabIndex = 0;
            this.labelHeader.Text = "Hashtag Analysis";
            // 
            // comboBoxHashtag
            // 
            this.comboBoxHashtag.FormattingEnabled = true;
            this.comboBoxHashtag.Location = new System.Drawing.Point(1136, 86);
            this.comboBoxHashtag.Name = "comboBoxHashtag";
            this.comboBoxHashtag.Size = new System.Drawing.Size(144, 21);
            this.comboBoxHashtag.TabIndex = 1;
            this.comboBoxHashtag.SelectedIndexChanged += new System.EventHandler(this.ComboBoxHashtag_SelectedIndexChanged);
            // 
            // cartesianChartStatus
            // 
            this.cartesianChartStatus.Location = new System.Drawing.Point(19, 468);
            this.cartesianChartStatus.Name = "cartesianChartStatus";
            this.cartesianChartStatus.Size = new System.Drawing.Size(407, 227);
            this.cartesianChartStatus.TabIndex = 2;
            this.cartesianChartStatus.Text = "cartesianChart1";
            // 
            // cartesianChartUser
            // 
            this.cartesianChartUser.Location = new System.Drawing.Point(447, 468);
            this.cartesianChartUser.Name = "cartesianChartUser";
            this.cartesianChartUser.Size = new System.Drawing.Size(407, 227);
            this.cartesianChartUser.TabIndex = 3;
            this.cartesianChartUser.Text = "cartesianChart1";
            // 
            // cartesianChartRetweet
            // 
            this.cartesianChartRetweet.Location = new System.Drawing.Point(877, 468);
            this.cartesianChartRetweet.Name = "cartesianChartRetweet";
            this.cartesianChartRetweet.Size = new System.Drawing.Size(407, 227);
            this.cartesianChartRetweet.TabIndex = 4;
            this.cartesianChartRetweet.Text = "cartesianChart1";
            // 
            // panelUserTotal
            // 
            this.panelUserTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelUserTotal.Controls.Add(this.pictureBoxUser);
            this.panelUserTotal.Controls.Add(this.labelUsersResult);
            this.panelUserTotal.Controls.Add(this.labelUserTotal);
            this.panelUserTotal.Location = new System.Drawing.Point(19, 292);
            this.panelUserTotal.Name = "panelUserTotal";
            this.panelUserTotal.Size = new System.Drawing.Size(165, 102);
            this.panelUserTotal.TabIndex = 5;
            // 
            // labelUsersResult
            // 
            this.labelUsersResult.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelUsersResult.ForeColor = System.Drawing.Color.White;
            this.labelUsersResult.Location = new System.Drawing.Point(47, 44);
            this.labelUsersResult.Name = "labelUsersResult";
            this.labelUsersResult.Size = new System.Drawing.Size(96, 26);
            this.labelUsersResult.TabIndex = 7;
            this.labelUsersResult.Text = "labelUsers";
            this.labelUsersResult.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelUsersResult.Click += new System.EventHandler(this.LabelUsersResult_Click);
            // 
            // labelUserTotal
            // 
            this.labelUserTotal.AutoSize = true;
            this.labelUserTotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelUserTotal.ForeColor = System.Drawing.Color.White;
            this.labelUserTotal.Location = new System.Drawing.Point(18, 18);
            this.labelUserTotal.Name = "labelUserTotal";
            this.labelUserTotal.Size = new System.Drawing.Size(43, 18);
            this.labelUserTotal.TabIndex = 6;
            this.labelUserTotal.Text = "Users";
            // 
            // panelRetweetTotal
            // 
            this.panelRetweetTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelRetweetTotal.Controls.Add(this.pictureBoxRetweets);
            this.panelRetweetTotal.Controls.Add(this.labelRetweetsResult);
            this.panelRetweetTotal.Controls.Add(this.labelRetweetTotal);
            this.panelRetweetTotal.Location = new System.Drawing.Point(207, 292);
            this.panelRetweetTotal.Name = "panelRetweetTotal";
            this.panelRetweetTotal.Size = new System.Drawing.Size(165, 102);
            this.panelRetweetTotal.TabIndex = 5;
            // 
            // labelRetweetsResult
            // 
            this.labelRetweetsResult.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelRetweetsResult.ForeColor = System.Drawing.Color.White;
            this.labelRetweetsResult.Location = new System.Drawing.Point(48, 44);
            this.labelRetweetsResult.Name = "labelRetweetsResult";
            this.labelRetweetsResult.Size = new System.Drawing.Size(96, 26);
            this.labelRetweetsResult.TabIndex = 7;
            this.labelRetweetsResult.Text = "labelRetweets";
            this.labelRetweetsResult.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelRetweetTotal
            // 
            this.labelRetweetTotal.AutoSize = true;
            this.labelRetweetTotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelRetweetTotal.ForeColor = System.Drawing.Color.White;
            this.labelRetweetTotal.Location = new System.Drawing.Point(20, 18);
            this.labelRetweetTotal.Name = "labelRetweetTotal";
            this.labelRetweetTotal.Size = new System.Drawing.Size(67, 18);
            this.labelRetweetTotal.TabIndex = 6;
            this.labelRetweetTotal.Text = "Retweets";
            // 
            // panelStatusTotal
            // 
            this.panelStatusTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelStatusTotal.Controls.Add(this.pictureBoxTweets);
            this.panelStatusTotal.Controls.Add(this.labelStatusResult);
            this.panelStatusTotal.Controls.Add(this.labelStatusTotal);
            this.panelStatusTotal.Location = new System.Drawing.Point(19, 172);
            this.panelStatusTotal.Name = "panelStatusTotal";
            this.panelStatusTotal.Size = new System.Drawing.Size(165, 102);
            this.panelStatusTotal.TabIndex = 5;
            // 
            // labelStatusResult
            // 
            this.labelStatusResult.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelStatusResult.ForeColor = System.Drawing.Color.White;
            this.labelStatusResult.Location = new System.Drawing.Point(47, 45);
            this.labelStatusResult.Name = "labelStatusResult";
            this.labelStatusResult.Size = new System.Drawing.Size(96, 26);
            this.labelStatusResult.TabIndex = 7;
            this.labelStatusResult.Text = "labelTweets";
            this.labelStatusResult.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelStatusTotal
            // 
            this.labelStatusTotal.AutoSize = true;
            this.labelStatusTotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelStatusTotal.ForeColor = System.Drawing.Color.White;
            this.labelStatusTotal.Location = new System.Drawing.Point(18, 17);
            this.labelStatusTotal.Name = "labelStatusTotal";
            this.labelStatusTotal.Size = new System.Drawing.Size(53, 18);
            this.labelStatusTotal.TabIndex = 6;
            this.labelStatusTotal.Text = "Tweets";
            // 
            // panelFavTotal
            // 
            this.panelFavTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelFavTotal.Controls.Add(this.pictureBoxFav);
            this.panelFavTotal.Controls.Add(this.labelFavResult);
            this.panelFavTotal.Controls.Add(this.labelFavTotal);
            this.panelFavTotal.Location = new System.Drawing.Point(207, 172);
            this.panelFavTotal.Name = "panelFavTotal";
            this.panelFavTotal.Size = new System.Drawing.Size(165, 102);
            this.panelFavTotal.TabIndex = 5;
            // 
            // labelFavResult
            // 
            this.labelFavResult.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelFavResult.ForeColor = System.Drawing.Color.White;
            this.labelFavResult.Location = new System.Drawing.Point(48, 45);
            this.labelFavResult.Name = "labelFavResult";
            this.labelFavResult.Size = new System.Drawing.Size(96, 26);
            this.labelFavResult.TabIndex = 7;
            this.labelFavResult.Text = "labelFav";
            this.labelFavResult.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFavTotal
            // 
            this.labelFavTotal.AutoSize = true;
            this.labelFavTotal.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            this.labelFavTotal.ForeColor = System.Drawing.Color.White;
            this.labelFavTotal.Location = new System.Drawing.Point(20, 17);
            this.labelFavTotal.Name = "labelFavTotal";
            this.labelFavTotal.Size = new System.Drawing.Size(59, 18);
            this.labelFavTotal.TabIndex = 6;
            this.labelFavTotal.Text = "Favorite";
            // 
            // labelChartStatus
            // 
            this.labelChartStatus.AutoSize = true;
            this.labelChartStatus.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labelChartStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.labelChartStatus.Location = new System.Drawing.Point(19, 434);
            this.labelChartStatus.Name = "labelChartStatus";
            this.labelChartStatus.Size = new System.Drawing.Size(120, 19);
            this.labelChartStatus.TabIndex = 6;
            this.labelChartStatus.Text = "Tweet Evolution";
            // 
            // labelChartUser
            // 
            this.labelChartUser.AutoSize = true;
            this.labelChartUser.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labelChartUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.labelChartUser.Location = new System.Drawing.Point(465, 434);
            this.labelChartUser.Name = "labelChartUser";
            this.labelChartUser.Size = new System.Drawing.Size(134, 19);
            this.labelChartUser.TabIndex = 6;
            this.labelChartUser.Text = "Favorite Evolution";
            // 
            // labelChartRetweet
            // 
            this.labelChartRetweet.AutoSize = true;
            this.labelChartRetweet.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labelChartRetweet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.labelChartRetweet.Location = new System.Drawing.Point(911, 434);
            this.labelChartRetweet.Name = "labelChartRetweet";
            this.labelChartRetweet.Size = new System.Drawing.Size(135, 19);
            this.labelChartRetweet.TabIndex = 6;
            this.labelChartRetweet.Text = "Retweet Evolution";
            // 
            // labelKPItit
            // 
            this.labelKPItit.AutoSize = true;
            this.labelKPItit.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold);
            this.labelKPItit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.labelKPItit.Location = new System.Drawing.Point(19, 132);
            this.labelKPItit.Name = "labelKPItit";
            this.labelKPItit.Size = new System.Drawing.Size(148, 19);
            this.labelKPItit.TabIndex = 6;
            this.labelKPItit.Text = "Total KPI\'s Summary";
            // 
            // panelHeader
            // 
            this.panelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(132)))), ((int)(((byte)(180)))));
            this.panelHeader.Controls.Add(this.imgClose);
            this.panelHeader.Controls.Add(this.imgMin);
            this.panelHeader.Controls.Add(this.imgReturn);
            this.panelHeader.Controls.Add(this.pictureBoxHeaderBird);
            this.panelHeader.Controls.Add(this.labelHeader);
            this.panelHeader.Location = new System.Drawing.Point(-2, -5);
            this.panelHeader.Name = "panelHeader";
            this.panelHeader.Size = new System.Drawing.Size(1687, 57);
            this.panelHeader.TabIndex = 7;
            // 
            // dataGridViewLastStatus
            // 
            this.dataGridViewLastStatus.AllowUserToAddRows = false;
            this.dataGridViewLastStatus.AllowUserToDeleteRows = false;
            this.dataGridViewLastStatus.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridViewLastStatus.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLastStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewLastStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLastStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.createddate,
            this.Location,
            this.tweet_link,
            this.tweet_text});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewLastStatus.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewLastStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridViewLastStatus.EnableHeadersVisualStyles = false;
            this.dataGridViewLastStatus.GridColor = System.Drawing.Color.White;
            this.dataGridViewLastStatus.Location = new System.Drawing.Point(469, 172);
            this.dataGridViewLastStatus.Name = "dataGridViewLastStatus";
            this.dataGridViewLastStatus.Size = new System.Drawing.Size(815, 222);
            this.dataGridViewLastStatus.TabIndex = 17;
            this.dataGridViewLastStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewLastStatus_CellContentClick);
            // 
            // labelComboHashtag
            // 
            this.labelComboHashtag.AutoSize = true;
            this.labelComboHashtag.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labelComboHashtag.Location = new System.Drawing.Point(1006, 86);
            this.labelComboHashtag.Name = "labelComboHashtag";
            this.labelComboHashtag.Size = new System.Drawing.Size(120, 18);
            this.labelComboHashtag.TabIndex = 18;
            this.labelComboHashtag.Text = "Choose a Hashtag:";
            // 
            // labelComboTime
            // 
            this.labelComboTime.AutoSize = true;
            this.labelComboTime.Font = new System.Drawing.Font("Calibri", 11.25F);
            this.labelComboTime.Location = new System.Drawing.Point(670, 86);
            this.labelComboTime.Name = "labelComboTime";
            this.labelComboTime.Size = new System.Drawing.Size(139, 18);
            this.labelComboTime.TabIndex = 18;
            this.labelComboTime.Text = "Choose a time frame:";
            this.labelComboTime.Click += new System.EventHandler(this.Label1_Click);
            // 
            // comboBoxTimeFrame
            // 
            this.comboBoxTimeFrame.FormattingEnabled = true;
            this.comboBoxTimeFrame.Location = new System.Drawing.Point(820, 86);
            this.comboBoxTimeFrame.Name = "comboBoxTimeFrame";
            this.comboBoxTimeFrame.Size = new System.Drawing.Size(144, 21);
            this.comboBoxTimeFrame.TabIndex = 1;
            this.comboBoxTimeFrame.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTimeFrame_SelectedIndexChanged);
            // 
            // labelHashtag
            // 
            this.labelHashtag.AutoSize = true;
            this.labelHashtag.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold);
            this.labelHashtag.Location = new System.Drawing.Point(91, 71);
            this.labelHashtag.Name = "labelHashtag";
            this.labelHashtag.Size = new System.Drawing.Size(113, 23);
            this.labelHashtag.TabIndex = 19;
            this.labelHashtag.Text = "labelHashtag";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Calibri", 9.75F);
            this.labelTime.Location = new System.Drawing.Point(92, 94);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(61, 15);
            this.labelTime.TabIndex = 19;
            this.labelTime.Text = "labelTime";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(25, 66);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // imgClose
            // 
            this.imgClose.BackgroundImage = global::TwitterAnalytics.Properties.Resources._296812_48;
            this.imgClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgClose.Location = new System.Drawing.Point(1274, 13);
            this.imgClose.Name = "imgClose";
            this.imgClose.Size = new System.Drawing.Size(20, 20);
            this.imgClose.TabIndex = 8;
            this.imgClose.TabStop = false;
            this.imgClose.Click += new System.EventHandler(this.ImgClose_Click);
            // 
            // imgMin
            // 
            this.imgMin.BackgroundImage = global::TwitterAnalytics.Properties.Resources._3838428_48;
            this.imgMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMin.Location = new System.Drawing.Point(1240, 13);
            this.imgMin.Name = "imgMin";
            this.imgMin.Size = new System.Drawing.Size(20, 20);
            this.imgMin.TabIndex = 8;
            this.imgMin.TabStop = false;
            this.imgMin.Click += new System.EventHandler(this.ImgMin_Click);
            // 
            // imgReturn
            // 
            this.imgReturn.BackgroundImage = global::TwitterAnalytics.Properties.Resources.return_48;
            this.imgReturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgReturn.Location = new System.Drawing.Point(1205, 13);
            this.imgReturn.Name = "imgReturn";
            this.imgReturn.Size = new System.Drawing.Size(20, 20);
            this.imgReturn.TabIndex = 8;
            this.imgReturn.TabStop = false;
            this.imgReturn.Click += new System.EventHandler(this.ImgReturn_Click);
            // 
            // pictureBoxHeaderBird
            // 
            this.pictureBoxHeaderBird.BackgroundImage = global::TwitterAnalytics.Properties.Resources._4518796_64_white;
            this.pictureBoxHeaderBird.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxHeaderBird.Location = new System.Drawing.Point(17, 10);
            this.pictureBoxHeaderBird.Name = "pictureBoxHeaderBird";
            this.pictureBoxHeaderBird.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxHeaderBird.TabIndex = 8;
            this.pictureBoxHeaderBird.TabStop = false;
            // 
            // pictureBoxFav
            // 
            this.pictureBoxFav.BackgroundImage = global::TwitterAnalytics.Properties.Resources.favimg;
            this.pictureBoxFav.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFav.Location = new System.Drawing.Point(9, 43);
            this.pictureBoxFav.Name = "pictureBoxFav";
            this.pictureBoxFav.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxFav.TabIndex = 21;
            this.pictureBoxFav.TabStop = false;
            // 
            // pictureBoxTweets
            // 
            this.pictureBoxTweets.BackgroundImage = global::TwitterAnalytics.Properties.Resources.tweet;
            this.pictureBoxTweets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxTweets.Location = new System.Drawing.Point(5, 38);
            this.pictureBoxTweets.Name = "pictureBoxTweets";
            this.pictureBoxTweets.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxTweets.TabIndex = 21;
            this.pictureBoxTweets.TabStop = false;
            // 
            // pictureBoxRetweets
            // 
            this.pictureBoxRetweets.BackgroundImage = global::TwitterAnalytics.Properties.Resources.retweetimg;
            this.pictureBoxRetweets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxRetweets.Location = new System.Drawing.Point(9, 46);
            this.pictureBoxRetweets.Name = "pictureBoxRetweets";
            this.pictureBoxRetweets.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxRetweets.TabIndex = 21;
            this.pictureBoxRetweets.TabStop = false;
            // 
            // pictureBoxUser
            // 
            this.pictureBoxUser.BackgroundImage = global::TwitterAnalytics.Properties.Resources.userimg;
            this.pictureBoxUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxUser.Location = new System.Drawing.Point(8, 46);
            this.pictureBoxUser.Name = "pictureBoxUser";
            this.pictureBoxUser.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxUser.TabIndex = 21;
            this.pictureBoxUser.TabStop = false;
            // 
            // createddate
            // 
            this.createddate.HeaderText = "Create Date";
            this.createddate.Name = "createddate";
            this.createddate.Width = 125;
            // 
            // Location
            // 
            this.Location.HeaderText = "Location";
            this.Location.Name = "Location";
            // 
            // tweet_link
            // 
            this.tweet_link.HeaderText = "Tweet Link";
            this.tweet_link.Name = "tweet_link";
            this.tweet_link.Width = 120;
            // 
            // tweet_text
            // 
            this.tweet_text.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.tweet_text.HeaderText = "Tweet";
            this.tweet_text.Name = "tweet_text";
            // 
            // fHashtag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1304, 703);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelHashtag);
            this.Controls.Add(this.labelComboTime);
            this.Controls.Add(this.labelComboHashtag);
            this.Controls.Add(this.dataGridViewLastStatus);
            this.Controls.Add(this.panelHeader);
            this.Controls.Add(this.labelChartRetweet);
            this.Controls.Add(this.labelChartUser);
            this.Controls.Add(this.labelKPItit);
            this.Controls.Add(this.labelChartStatus);
            this.Controls.Add(this.panelFavTotal);
            this.Controls.Add(this.panelStatusTotal);
            this.Controls.Add(this.panelRetweetTotal);
            this.Controls.Add(this.panelUserTotal);
            this.Controls.Add(this.cartesianChartRetweet);
            this.Controls.Add(this.cartesianChartUser);
            this.Controls.Add(this.cartesianChartStatus);
            this.Controls.Add(this.comboBoxTimeFrame);
            this.Controls.Add(this.comboBoxHashtag);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fHashtag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panelUserTotal.ResumeLayout(false);
            this.panelUserTotal.PerformLayout();
            this.panelRetweetTotal.ResumeLayout(false);
            this.panelRetweetTotal.PerformLayout();
            this.panelStatusTotal.ResumeLayout(false);
            this.panelStatusTotal.PerformLayout();
            this.panelFavTotal.ResumeLayout(false);
            this.panelFavTotal.PerformLayout();
            this.panelHeader.ResumeLayout(false);
            this.panelHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLastStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeaderBird)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTweets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRetweets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelHeader;
        private System.Windows.Forms.ComboBox comboBoxHashtag;
        private LiveCharts.WinForms.CartesianChart cartesianChartStatus;
        private LiveCharts.WinForms.CartesianChart cartesianChartUser;
        private LiveCharts.WinForms.CartesianChart cartesianChartRetweet;
        private System.Windows.Forms.Panel panelUserTotal;
        private System.Windows.Forms.Panel panelRetweetTotal;
        private System.Windows.Forms.Panel panelStatusTotal;
        private System.Windows.Forms.Panel panelFavTotal;
        private System.Windows.Forms.Label labelStatusTotal;
        private System.Windows.Forms.Label labelUserTotal;
        private System.Windows.Forms.Label labelFavTotal;
        private System.Windows.Forms.Label labelRetweetTotal;
        private System.Windows.Forms.Label labelChartStatus;
        private System.Windows.Forms.Label labelChartUser;
        private System.Windows.Forms.Label labelChartRetweet;
        private System.Windows.Forms.Label labelKPItit;
        private System.Windows.Forms.Panel panelHeader;
        private System.Windows.Forms.PictureBox pictureBoxHeaderBird;
        private System.Windows.Forms.PictureBox imgReturn;
        private System.Windows.Forms.PictureBox imgMin;
        private System.Windows.Forms.PictureBox imgClose;
        private System.Windows.Forms.DataGridView dataGridViewLastStatus;
        private System.Windows.Forms.Label labelComboHashtag;
        private System.Windows.Forms.Label labelComboTime;
        private System.Windows.Forms.ComboBox comboBoxTimeFrame;
        private System.Windows.Forms.Label labelHashtag;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelStatusResult;
        private System.Windows.Forms.Label labelUsersResult;
        private System.Windows.Forms.Label labelRetweetsResult;
        private System.Windows.Forms.Label labelFavResult;
        private System.Windows.Forms.PictureBox pictureBoxUser;
        private System.Windows.Forms.PictureBox pictureBoxRetweets;
        private System.Windows.Forms.PictureBox pictureBoxTweets;
        private System.Windows.Forms.PictureBox pictureBoxFav;
        private System.Windows.Forms.DataGridViewTextBoxColumn createddate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Location;
        private System.Windows.Forms.DataGridViewLinkColumn tweet_link;
        private System.Windows.Forms.DataGridViewTextBoxColumn tweet_text;
    }
}