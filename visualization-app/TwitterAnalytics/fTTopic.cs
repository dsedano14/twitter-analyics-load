﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TwitterAnalytics.Model;
using LiveCharts;
using LiveCharts.Wpf;

namespace TwitterAnalytics
{
    public partial class fTTopic : Form
    {
        private fMain mainform;
        Trend result_general = new Trend();
        List<TrendTop> trendtop = new List<TrendTop>();
        List<TrendRepeated> trend_repeated = new List<TrendRepeated>();
        TrendTop resultado = new TrendTop();


        public fTTopic(TwitterAnalytics.fMain p, object trends)
        {
            InitializeComponent();
            this.Owner = p;
            mainform = p;

            result_general = (Trend)trends;

            //result = (List<TrendTop>)trends;
            trendtop = result_general.trends_array;
            trend_repeated = result_general.trends_repeated;


            FillComboBoxDate(trendtop);
            FillComboBoxPlace(trendtop);
            comboBoxDate.SelectedItem = trendtop[0].day;
            comboBoxPlace.SelectedItem = trendtop[0].place;
            //FillPieCHart(result);
        }

        private void FillPieCHart(List<TrendTop> pielist)
        {
            pieChart1.Series.Clear(); 
            List<TrendTop> filteredList = pielist.Where(x => x.place.Equals(comboBoxPlace.SelectedItem) & x.day.Equals(comboBoxDate.SelectedItem)).ToList();

            pieChart1.InnerRadius = 100;
            pieChart1.LegendLocation = LegendLocation.Right;

            foreach (TrendTop tt in filteredList)
            {
                pieChart1.Series.Add(new PieSeries
                {
                    Title = tt.name,
                    Values = new ChartValues<double> { Double.Parse(tt.tweet_volume) }
                });
            }

            if(!(filteredList.Count()==0))
            FillLabels(filteredList);

        }

        private void FillBarChart(List<TrendRepeated> trendrepeated)
        {
            List<TrendRepeated> filteredList = trendrepeated.Where(x => x.trend_place.Equals(comboBoxPlace.SelectedItem)).ToList();
            List<String> asa = new List<String>();
            ChartValues<double> chartvalues = new ChartValues<double> ();

            cartesianChart1.Series.Clear();
            cartesianChart1.AxisX.Clear();


             foreach (TrendRepeated ff in filteredList)
            {

                cartesianChart1.Series.Add(new ColumnSeries
                {
                    DataLabels = true,
                    Title = ff.trend_name,
                    Values = new ChartValues<double> { Double.Parse(ff.num_repeated) }

                });



            }



            cartesianChart1.AxisX.Add(new Axis
            {
                Title = "",
                //Labels = asa
                Labels = new[] { "Trends" }
            }) ;
            

        }

            private void FillLabels(List<TrendTop> ttlist)
        {
            labelTitle2.Text = ttlist[0].place+" Trends";

            dataGridView1.Rows.Clear();
            dataGridView1.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridView1.RowHeadersVisible = false;
            dataGridView1.EnableHeadersVisualStyles = false;


            //dataGridViewLastStatus.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(155, 210, 255);

            dataGridView1.RowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(218, 238, 255);
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.White;


            foreach (TrendTop tt in ttlist)
            {
                int n = dataGridView1.Rows.Add();

                dataGridView1.Rows[n].Cells[0].Value = tt.name;
                dataGridView1.Rows[n].Cells[1].Value = tt.query;
                dataGridView1.Rows[n].Cells[2].Value = tt.tweet_volume;
            }

            dataGridView1.Rows[0].Cells[0].Selected = false;

        }

        private void FillComboBoxDate(List<TrendTop> entryList)
        {

            foreach (TrendTop tt in entryList)
            {
                if (!comboBoxDate.Items.Contains(tt.day))
                {
                    comboBoxDate.Items.Add(tt.day);
                }                
            }
        }

        private void FillComboBoxPlace(List<TrendTop> entryList)
        {

            foreach (TrendTop tt in entryList)
            {
                if (!comboBoxPlace.Items.Contains(tt.place))
                {
                    comboBoxPlace.Items.Add(tt.place);
                }
            }
        }

        private void ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillPieCHart(trendtop);
            FillBarChart(trend_repeated);
        }

        private void DataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedCells[0].ColumnIndex.Equals(1))
            {
                System.Diagnostics.Process.Start(dataGridView1.SelectedCells[0].Value.ToString());
            }

        }

        private void ImgReturn_Click(object sender, EventArgs e)
        {
            mainform.Show();
            this.Close();
        }

        private void ImgMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ImgClose_Click(object sender, EventArgs e)
        {
            mainform.Show();
            this.Close();
        }
    }
}
