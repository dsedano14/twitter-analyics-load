﻿namespace TwitterAnalytics
{
    partial class UserAnalysis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserAnalysis));
            this.cartesianChartFollower = new LiveCharts.WinForms.CartesianChart();
            this.comboBoxUser = new System.Windows.Forms.ComboBox();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.cartesianChartFriend = new LiveCharts.Wpf.CartesianChart();
            this.elementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.cartesianChartFavorite = new LiveCharts.Wpf.CartesianChart();
            this.labelName = new System.Windows.Forms.Label();
            this.labelDesc = new System.Windows.Forms.Label();
            this.labelLocation = new System.Windows.Forms.Label();
            this.linkLabelurl = new System.Windows.Forms.LinkLabel();
            this.labelTweets = new System.Windows.Forms.Label();
            this.labelFollower = new System.Windows.Forms.Label();
            this.labelFriend = new System.Windows.Forms.Label();
            this.labelFavorite = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgClose2 = new System.Windows.Forms.PictureBox();
            this.imgReturn = new System.Windows.Forms.PictureBox();
            this.imgMin2 = new System.Windows.Forms.PictureBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelKpiFoll = new System.Windows.Forms.Panel();
            this.labelTitFollower = new System.Windows.Forms.Label();
            this.pictureBoxFollower = new System.Windows.Forms.PictureBox();
            this.panelKpiTweets = new System.Windows.Forms.Panel();
            this.pictureBoxTweets = new System.Windows.Forms.PictureBox();
            this.labelTitTweets = new System.Windows.Forms.Label();
            this.panelKPIFriend = new System.Windows.Forms.Panel();
            this.pictureBoxFriend = new System.Windows.Forms.PictureBox();
            this.labelTitFriend = new System.Windows.Forms.Label();
            this.panelKpiFavorite = new System.Windows.Forms.Panel();
            this.pictureBoxFav = new System.Windows.Forms.PictureBox();
            this.labelTitFav = new System.Windows.Forms.Label();
            this.labelSelector = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dataGridViewLastStatus = new System.Windows.Forms.DataGridView();
            this.CreatedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.retweet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.favorite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.link = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Tweet = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBoxPhoto = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReturn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin2)).BeginInit();
            this.panelKpiFoll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFollower)).BeginInit();
            this.panelKpiTweets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTweets)).BeginInit();
            this.panelKPIFriend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriend)).BeginInit();
            this.panelKpiFavorite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFav)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLastStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).BeginInit();
            this.SuspendLayout();
            // 
            // cartesianChartFollower
            // 
            this.cartesianChartFollower.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cartesianChartFollower.Location = new System.Drawing.Point(17, 453);
            this.cartesianChartFollower.Name = "cartesianChartFollower";
            this.cartesianChartFollower.Size = new System.Drawing.Size(407, 227);
            this.cartesianChartFollower.TabIndex = 2;
            this.cartesianChartFollower.Text = "cartesianChart1";
            // 
            // comboBoxUser
            // 
            this.comboBoxUser.FormattingEnabled = true;
            this.comboBoxUser.Location = new System.Drawing.Point(1074, 65);
            this.comboBoxUser.Name = "comboBoxUser";
            this.comboBoxUser.Size = new System.Drawing.Size(201, 21);
            this.comboBoxUser.TabIndex = 3;
            this.comboBoxUser.SelectedIndexChanged += new System.EventHandler(this.ComboBoxUser_SelectedIndexChanged);
            // 
            // elementHost1
            // 
            this.elementHost1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elementHost1.Location = new System.Drawing.Point(442, 453);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(407, 227);
            this.elementHost1.TabIndex = 4;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = this.cartesianChartFriend;
            // 
            // elementHost2
            // 
            this.elementHost2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.elementHost2.Location = new System.Drawing.Point(868, 453);
            this.elementHost2.Name = "elementHost2";
            this.elementHost2.Size = new System.Drawing.Size(407, 227);
            this.elementHost2.TabIndex = 5;
            this.elementHost2.Text = "elementHost2";
            this.elementHost2.Child = this.cartesianChartFavorite;
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelName.Location = new System.Drawing.Point(87, 64);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(96, 23);
            this.labelName.TabIndex = 6;
            this.labelName.Text = "labelName";
            // 
            // labelDesc
            // 
            this.labelDesc.AutoSize = true;
            this.labelDesc.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDesc.Location = new System.Drawing.Point(88, 90);
            this.labelDesc.Name = "labelDesc";
            this.labelDesc.Size = new System.Drawing.Size(61, 15);
            this.labelDesc.TabIndex = 7;
            this.labelDesc.Text = "labelDesc";
            // 
            // labelLocation
            // 
            this.labelLocation.AutoSize = true;
            this.labelLocation.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLocation.Location = new System.Drawing.Point(9, 138);
            this.labelLocation.Name = "labelLocation";
            this.labelLocation.Size = new System.Drawing.Size(81, 14);
            this.labelLocation.TabIndex = 8;
            this.labelLocation.Text = "labelLocation";
            // 
            // linkLabelurl
            // 
            this.linkLabelurl.AutoSize = true;
            this.linkLabelurl.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabelurl.Location = new System.Drawing.Point(9, 108);
            this.linkLabelurl.Name = "linkLabelurl";
            this.linkLabelurl.Size = new System.Drawing.Size(63, 13);
            this.linkLabelurl.TabIndex = 10;
            this.linkLabelurl.TabStop = true;
            this.linkLabelurl.Text = "linkLabelurl";
            this.linkLabelurl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabelurl_LinkClicked);
            // 
            // labelTweets
            // 
            this.labelTweets.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelTweets.ForeColor = System.Drawing.Color.White;
            this.labelTweets.Location = new System.Drawing.Point(61, 46);
            this.labelTweets.Name = "labelTweets";
            this.labelTweets.Size = new System.Drawing.Size(92, 26);
            this.labelTweets.TabIndex = 11;
            this.labelTweets.Text = "labelTweets";
            this.labelTweets.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFollower
            // 
            this.labelFollower.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFollower.ForeColor = System.Drawing.Color.White;
            this.labelFollower.Location = new System.Drawing.Point(57, 42);
            this.labelFollower.Name = "labelFollower";
            this.labelFollower.Size = new System.Drawing.Size(97, 26);
            this.labelFollower.TabIndex = 12;
            this.labelFollower.Text = "labelFollower";
            this.labelFollower.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFriend
            // 
            this.labelFriend.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelFriend.ForeColor = System.Drawing.Color.White;
            this.labelFriend.Location = new System.Drawing.Point(62, 40);
            this.labelFriend.Name = "labelFriend";
            this.labelFriend.Size = new System.Drawing.Size(92, 26);
            this.labelFriend.TabIndex = 12;
            this.labelFriend.Text = "labelFriend";
            this.labelFriend.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labelFavorite
            // 
            this.labelFavorite.Font = new System.Drawing.Font("Calibri", 15.75F);
            this.labelFavorite.ForeColor = System.Drawing.Color.White;
            this.labelFavorite.Location = new System.Drawing.Point(61, 40);
            this.labelFavorite.Name = "labelFavorite";
            this.labelFavorite.Size = new System.Drawing.Size(92, 26);
            this.labelFavorite.TabIndex = 12;
            this.labelFavorite.Text = "labelFavorite";
            this.labelFavorite.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(132)))), ((int)(((byte)(180)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.imgClose2);
            this.panel1.Controls.Add(this.imgReturn);
            this.panel1.Controls.Add(this.imgMin2);
            this.panel1.Controls.Add(this.labelTitle);
            this.panel1.Location = new System.Drawing.Point(-2, -5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1687, 57);
            this.panel1.TabIndex = 13;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::TwitterAnalytics.Properties.Resources._4518796_64_white;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(17, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 40);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            // 
            // imgClose2
            // 
            this.imgClose2.BackColor = System.Drawing.Color.Transparent;
            this.imgClose2.BackgroundImage = global::TwitterAnalytics.Properties.Resources._296812_48;
            this.imgClose2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgClose2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgClose2.Location = new System.Drawing.Point(1274, 13);
            this.imgClose2.Name = "imgClose2";
            this.imgClose2.Size = new System.Drawing.Size(20, 20);
            this.imgClose2.TabIndex = 17;
            this.imgClose2.TabStop = false;
            this.imgClose2.Click += new System.EventHandler(this.ImgClose_Click);
            // 
            // imgReturn
            // 
            this.imgReturn.BackgroundImage = global::TwitterAnalytics.Properties.Resources.return_48;
            this.imgReturn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgReturn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgReturn.Location = new System.Drawing.Point(1205, 13);
            this.imgReturn.Name = "imgReturn";
            this.imgReturn.Size = new System.Drawing.Size(20, 20);
            this.imgReturn.TabIndex = 17;
            this.imgReturn.TabStop = false;
            this.imgReturn.Click += new System.EventHandler(this.ImgReturn_Click);
            // 
            // imgMin2
            // 
            this.imgMin2.BackColor = System.Drawing.Color.Transparent;
            this.imgMin2.BackgroundImage = global::TwitterAnalytics.Properties.Resources._3838428_48;
            this.imgMin2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgMin2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMin2.Location = new System.Drawing.Point(1240, 13);
            this.imgMin2.Name = "imgMin2";
            this.imgMin2.Size = new System.Drawing.Size(20, 20);
            this.imgMin2.TabIndex = 17;
            this.imgMin2.TabStop = false;
            this.imgMin2.Click += new System.EventHandler(this.ImgMin_Click);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(73, 20);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(97, 19);
            this.labelTitle.TabIndex = 0;
            this.labelTitle.Text = "User Analysis";
            // 
            // panelKpiFoll
            // 
            this.panelKpiFoll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelKpiFoll.Controls.Add(this.labelTitFollower);
            this.panelKpiFoll.Controls.Add(this.labelFollower);
            this.panelKpiFoll.Controls.Add(this.pictureBoxFollower);
            this.panelKpiFoll.Location = new System.Drawing.Point(23, 196);
            this.panelKpiFoll.Name = "panelKpiFoll";
            this.panelKpiFoll.Size = new System.Drawing.Size(165, 102);
            this.panelKpiFoll.TabIndex = 14;
            // 
            // labelTitFollower
            // 
            this.labelTitFollower.AutoSize = true;
            this.labelTitFollower.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitFollower.ForeColor = System.Drawing.Color.White;
            this.labelTitFollower.Location = new System.Drawing.Point(14, 10);
            this.labelTitFollower.Name = "labelTitFollower";
            this.labelTitFollower.Size = new System.Drawing.Size(69, 18);
            this.labelTitFollower.TabIndex = 17;
            this.labelTitFollower.Text = "Followers";
            // 
            // pictureBoxFollower
            // 
            this.pictureBoxFollower.BackgroundImage = global::TwitterAnalytics.Properties.Resources.follower_48;
            this.pictureBoxFollower.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFollower.Location = new System.Drawing.Point(7, 36);
            this.pictureBoxFollower.Name = "pictureBoxFollower";
            this.pictureBoxFollower.Size = new System.Drawing.Size(55, 55);
            this.pictureBoxFollower.TabIndex = 17;
            this.pictureBoxFollower.TabStop = false;
            // 
            // panelKpiTweets
            // 
            this.panelKpiTweets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelKpiTweets.Controls.Add(this.pictureBoxTweets);
            this.panelKpiTweets.Controls.Add(this.labelTitTweets);
            this.panelKpiTweets.Controls.Add(this.labelTweets);
            this.panelKpiTweets.Location = new System.Drawing.Point(219, 196);
            this.panelKpiTweets.Name = "panelKpiTweets";
            this.panelKpiTweets.Size = new System.Drawing.Size(165, 102);
            this.panelKpiTweets.TabIndex = 15;
            // 
            // pictureBoxTweets
            // 
            this.pictureBoxTweets.BackgroundImage = global::TwitterAnalytics.Properties.Resources.tweet_48;
            this.pictureBoxTweets.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxTweets.Location = new System.Drawing.Point(17, 46);
            this.pictureBoxTweets.Name = "pictureBoxTweets";
            this.pictureBoxTweets.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxTweets.TabIndex = 17;
            this.pictureBoxTweets.TabStop = false;
            // 
            // labelTitTweets
            // 
            this.labelTitTweets.AutoSize = true;
            this.labelTitTweets.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.labelTitTweets.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitTweets.ForeColor = System.Drawing.Color.White;
            this.labelTitTweets.Location = new System.Drawing.Point(19, 10);
            this.labelTitTweets.Name = "labelTitTweets";
            this.labelTitTweets.Size = new System.Drawing.Size(53, 18);
            this.labelTitTweets.TabIndex = 17;
            this.labelTitTweets.Text = "Tweets";
            // 
            // panelKPIFriend
            // 
            this.panelKPIFriend.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelKPIFriend.Controls.Add(this.pictureBoxFriend);
            this.panelKPIFriend.Controls.Add(this.labelTitFriend);
            this.panelKPIFriend.Controls.Add(this.labelFriend);
            this.panelKPIFriend.Location = new System.Drawing.Point(23, 311);
            this.panelKPIFriend.Name = "panelKPIFriend";
            this.panelKPIFriend.Size = new System.Drawing.Size(165, 102);
            this.panelKPIFriend.TabIndex = 15;
            // 
            // pictureBoxFriend
            // 
            this.pictureBoxFriend.BackgroundImage = global::TwitterAnalytics.Properties.Resources.friends_48;
            this.pictureBoxFriend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFriend.Location = new System.Drawing.Point(10, 40);
            this.pictureBoxFriend.Name = "pictureBoxFriend";
            this.pictureBoxFriend.Size = new System.Drawing.Size(45, 50);
            this.pictureBoxFriend.TabIndex = 17;
            this.pictureBoxFriend.TabStop = false;
            // 
            // labelTitFriend
            // 
            this.labelTitFriend.AutoSize = true;
            this.labelTitFriend.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitFriend.ForeColor = System.Drawing.Color.White;
            this.labelTitFriend.Location = new System.Drawing.Point(14, 11);
            this.labelTitFriend.Name = "labelTitFriend";
            this.labelTitFriend.Size = new System.Drawing.Size(54, 18);
            this.labelTitFriend.TabIndex = 17;
            this.labelTitFriend.Text = "Friends";
            // 
            // panelKpiFavorite
            // 
            this.panelKpiFavorite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(210)))), ((int)(((byte)(255)))));
            this.panelKpiFavorite.Controls.Add(this.pictureBoxFav);
            this.panelKpiFavorite.Controls.Add(this.labelTitFav);
            this.panelKpiFavorite.Controls.Add(this.labelFavorite);
            this.panelKpiFavorite.Location = new System.Drawing.Point(219, 311);
            this.panelKpiFavorite.Name = "panelKpiFavorite";
            this.panelKpiFavorite.Size = new System.Drawing.Size(165, 102);
            this.panelKpiFavorite.TabIndex = 15;
            // 
            // pictureBoxFav
            // 
            this.pictureBoxFav.BackgroundImage = global::TwitterAnalytics.Properties.Resources.fav_48;
            this.pictureBoxFav.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxFav.Location = new System.Drawing.Point(16, 45);
            this.pictureBoxFav.Name = "pictureBoxFav";
            this.pictureBoxFav.Size = new System.Drawing.Size(40, 40);
            this.pictureBoxFav.TabIndex = 17;
            this.pictureBoxFav.TabStop = false;
            // 
            // labelTitFav
            // 
            this.labelTitFav.AutoSize = true;
            this.labelTitFav.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitFav.ForeColor = System.Drawing.Color.White;
            this.labelTitFav.Location = new System.Drawing.Point(19, 11);
            this.labelTitFav.Name = "labelTitFav";
            this.labelTitFav.Size = new System.Drawing.Size(65, 18);
            this.labelTitFav.TabIndex = 17;
            this.labelTitFav.Text = "Favorites";
            // 
            // labelSelector
            // 
            this.labelSelector.AutoSize = true;
            this.labelSelector.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSelector.Location = new System.Drawing.Point(949, 66);
            this.labelSelector.Name = "labelSelector";
            this.labelSelector.Size = new System.Drawing.Size(112, 18);
            this.labelSelector.TabIndex = 16;
            this.labelSelector.Text = "Choose a profile:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.label2.Location = new System.Drawing.Point(24, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 19);
            this.label2.TabIndex = 6;
            this.label2.Text = "KPI\'s Summary";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.label1.Location = new System.Drawing.Point(24, 431);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 19);
            this.label1.TabIndex = 6;
            this.label1.Text = "Follower Evolution";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.label3.Location = new System.Drawing.Point(450, 431);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 19);
            this.label3.TabIndex = 6;
            this.label3.Text = "Friends Evolution";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.label4.Location = new System.Drawing.Point(877, 431);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 19);
            this.label4.TabIndex = 6;
            this.label4.Text = "Favorite Evolution";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            this.label5.Location = new System.Drawing.Point(450, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 19);
            this.label5.TabIndex = 6;
            this.label5.Text = "Latest Tweets";
            // 
            // dataGridViewLastStatus
            // 
            this.dataGridViewLastStatus.AllowUserToAddRows = false;
            this.dataGridViewLastStatus.AllowUserToDeleteRows = false;
            this.dataGridViewLastStatus.AllowUserToOrderColumns = true;
            this.dataGridViewLastStatus.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.dataGridViewLastStatus.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(157)))), ((int)(((byte)(243)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLastStatus.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewLastStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewLastStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CreatedDate,
            this.retweet,
            this.favorite,
            this.link,
            this.Tweet});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewLastStatus.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewLastStatus.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2;
            this.dataGridViewLastStatus.EnableHeadersVisualStyles = false;
            this.dataGridViewLastStatus.GridColor = System.Drawing.Color.White;
            this.dataGridViewLastStatus.Location = new System.Drawing.Point(454, 196);
            this.dataGridViewLastStatus.Name = "dataGridViewLastStatus";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(148)))), ((int)(((byte)(243)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewLastStatus.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewLastStatus.Size = new System.Drawing.Size(821, 217);
            this.dataGridViewLastStatus.TabIndex = 17;
            this.dataGridViewLastStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewLastStatus_CellContentClick);
            // 
            // CreatedDate
            // 
            this.CreatedDate.HeaderText = "Created Date";
            this.CreatedDate.Name = "CreatedDate";
            this.CreatedDate.Width = 125;
            // 
            // retweet
            // 
            this.retweet.HeaderText = "Nº Retweet";
            this.retweet.Name = "retweet";
            this.retweet.Width = 60;
            // 
            // favorite
            // 
            this.favorite.HeaderText = "Nº Favorite";
            this.favorite.Name = "favorite";
            this.favorite.Width = 60;
            // 
            // link
            // 
            this.link.HeaderText = "Tweet Link";
            this.link.Name = "link";
            this.link.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.link.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.link.Width = 120;
            // 
            // Tweet
            // 
            this.Tweet.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Tweet.HeaderText = "Tweet";
            this.Tweet.Name = "Tweet";
            // 
            // pictureBoxPhoto
            // 
            this.pictureBoxPhoto.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPhoto.Location = new System.Drawing.Point(12, 58);
            this.pictureBoxPhoto.Name = "pictureBoxPhoto";
            this.pictureBoxPhoto.Size = new System.Drawing.Size(47, 47);
            this.pictureBoxPhoto.TabIndex = 9;
            this.pictureBoxPhoto.TabStop = false;
            // 
            // UserAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1304, 703);
            this.Controls.Add(this.dataGridViewLastStatus);
            this.Controls.Add(this.labelSelector);
            this.Controls.Add(this.panelKPIFriend);
            this.Controls.Add(this.elementHost1);
            this.Controls.Add(this.panelKpiFavorite);
            this.Controls.Add(this.panelKpiTweets);
            this.Controls.Add(this.panelKpiFoll);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.linkLabelurl);
            this.Controls.Add(this.pictureBoxPhoto);
            this.Controls.Add(this.labelLocation);
            this.Controls.Add(this.labelDesc);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelName);
            this.Controls.Add(this.elementHost2);
            this.Controls.Add(this.comboBoxUser);
            this.Controls.Add(this.cartesianChartFollower);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UserAnalysis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "UserAnalysis";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgReturn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin2)).EndInit();
            this.panelKpiFoll.ResumeLayout(false);
            this.panelKpiFoll.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFollower)).EndInit();
            this.panelKpiTweets.ResumeLayout(false);
            this.panelKpiTweets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTweets)).EndInit();
            this.panelKPIFriend.ResumeLayout(false);
            this.panelKPIFriend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFriend)).EndInit();
            this.panelKpiFavorite.ResumeLayout(false);
            this.panelKpiFavorite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFav)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewLastStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPhoto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private LiveCharts.WinForms.CartesianChart cartesianChartFollower;
        private System.Windows.Forms.ComboBox comboBoxUser;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private LiveCharts.Wpf.CartesianChart cartesianChartFriend;
        private System.Windows.Forms.Integration.ElementHost elementHost2;
        private LiveCharts.Wpf.CartesianChart cartesianChartFavorite;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelDesc;
        private System.Windows.Forms.Label labelLocation;
        private System.Windows.Forms.PictureBox pictureBoxPhoto;
        private System.Windows.Forms.LinkLabel linkLabelurl;
        private System.Windows.Forms.Label labelTweets;
        private System.Windows.Forms.Label labelFollower;
        private System.Windows.Forms.Label labelFriend;
        private System.Windows.Forms.Label labelFavorite;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Panel panelKpiFoll;
        private System.Windows.Forms.Panel panelKpiTweets;
        private System.Windows.Forms.Panel panelKPIFriend;
        private System.Windows.Forms.Panel panelKpiFavorite;
        private System.Windows.Forms.Label labelSelector;
        private System.Windows.Forms.PictureBox imgReturn;
        private System.Windows.Forms.Label labelTitFollower;
        private System.Windows.Forms.Label labelTitTweets;
        private System.Windows.Forms.Label labelTitFriend;
        private System.Windows.Forms.Label labelTitFav;
        private System.Windows.Forms.PictureBox pictureBoxFollower;
        private System.Windows.Forms.PictureBox pictureBoxTweets;
        private System.Windows.Forms.PictureBox pictureBoxFriend;
        private System.Windows.Forms.PictureBox pictureBoxFav;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox imgClose2;
        private System.Windows.Forms.PictureBox imgMin2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridViewLastStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn retweet;
        private System.Windows.Forms.DataGridViewTextBoxColumn favorite;
        private System.Windows.Forms.DataGridViewLinkColumn link;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tweet;
    }
}