﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TwitterAnalytics.Model;
using LiveCharts; //Core of the library
using LiveCharts.Wpf; //The WPF controls
using LiveCharts.WinForms; //the WinForm wrappers
using System.Windows.Media;
using Brushes = System.Windows.Media.Brushes;

namespace TwitterAnalytics
{
    public partial class fHashtag : Form
    {
        private fMain gg;
        Hashtag result = new Hashtag();
        Dictionary<string, List<HashtagEvolution>> evolution_dictionary = new Dictionary<string, List<HashtagEvolution>>();
        List<HashtagEvolution> evolution_array = new List<HashtagEvolution>();
        List<HashtagLastStatus> last_status_array = new List<HashtagLastStatus>();

        public fHashtag(TwitterAnalytics.fMain p, object hashtag)
        {
            InitializeComponent();
            this.Owner = p;
            gg = p;

            //convierto objeto pasado a Hashtag
            result = (Hashtag)hashtag;

            //asigno a dictionary de evolucion
            evolution_dictionary = result.hashtag_evolution;

            //asignamos valor a last status
            last_status_array = result.last_status;

            Console.WriteLine(evolution_dictionary);
            //llenamos y asignamos valir a combobox time frame
            FillComboBoxTimeFrame(evolution_dictionary);
            comboBoxTimeFrame.SelectedItem = comboBoxTimeFrame.Items[0];
        }

        private void FillComboBoxHashtag(List<HashtagEvolution> entryList)
        {
            foreach (HashtagEvolution ht in entryList)
            {
                if (!comboBoxHashtag.Items.Contains(ht.name))
                {
                    comboBoxHashtag.Items.Add(ht.name);
                }
            }
        }

        private void FillComboBoxTimeFrame(Dictionary<string, List<HashtagEvolution>> dic_list)
        {

            foreach (KeyValuePair<string, List<HashtagEvolution>> kv in dic_list)
            {
                comboBoxTimeFrame.Items.Add(kv.Key);
            }
        }

        private void FillStatusChart(List<HashtagEvolution> result)
        {
            cartesianChartStatus.Series.Clear();
            cartesianChartStatus.AxisX.Clear();
            cartesianChartStatus.AxisY.Clear();

            cartesianChartStatus.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Tweets",
                    Values = EvoltoChart("status",result)
                }
            };

            cartesianChartStatus.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = EvoltoArray(result)
            });

            cartesianChartStatus.AxisY.Add(new Axis
            {
                Title = "Nº Followers"
            });

            cartesianChartStatus.LegendLocation = LegendLocation.None;


            //cartesianChartStatus.DataClick += cartesianChartStatusOnDataClick;
        }

        private void FillUserChart(List<HashtagEvolution> result)
        {
            cartesianChartUser.Series.Clear();
            cartesianChartUser.AxisX.Clear();
            cartesianChartUser.AxisY.Clear();

            cartesianChartUser.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Favorite",
                    Values = EvoltoChart("user",result)
                }
            };

            cartesianChartUser.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = EvoltoArray(result)
            });

            cartesianChartUser.AxisY.Add(new Axis
            {
                Title = "Nº Fav"
            });

            cartesianChartUser.LegendLocation = LegendLocation.None;


            //cartesianChartUser.DataClick += cartesianChartStatusOnDataClick;
        }

        private void FillRetweetChart(List<HashtagEvolution> result)
        {
            cartesianChartRetweet.Series.Clear();
            cartesianChartRetweet.AxisX.Clear();
            cartesianChartRetweet.AxisY.Clear();

            cartesianChartRetweet.Series = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Retweet",
                    Values = EvoltoChart("retweet",result)
                }
            };

            cartesianChartRetweet.AxisX.Add(new Axis
            {
                Title = "Date",
                Labels = EvoltoArray(result)
            });

            cartesianChartRetweet.AxisY.Add(new Axis
            {
                Title = "Nº Retweet"
            });

            cartesianChartRetweet.LegendLocation = LegendLocation.None;


            //cartesianChartRetweet.DataClick += cartesianChartStatusOnDataClick;
        }

        private ChartValues<double> EvoltoChart(string type, List<HashtagEvolution> arrayToTransform)
        {
            ChartValues<double> final_array = new ChartValues<double>();

            foreach (HashtagEvolution he in arrayToTransform)
            {
                if(type.Equals("status"))
                    final_array.Add(Convert.ToDouble(he.num_status));
                else if(type.Equals("user"))
                    final_array.Add(Convert.ToDouble(he.num_favorite));
                else
                    final_array.Add(Convert.ToDouble(he.num_retweets));

            }

            return final_array;

        }
        private string[] EvoltoArray(List<HashtagEvolution> arrayToTransform)
        {
            List<string> aux = new List<string>();

            foreach(HashtagEvolution he in arrayToTransform)
            {
                aux.Add(he.time);
            }

            return aux.ToArray();

        }

        private void cartesianChartStatusOnDataClick(object sender, ChartPoint chartPoint)
        {
            MessageBox.Show("You clicked (" + chartPoint.X + "," + chartPoint.Y + ")");
        }

        private void ComboBoxHashtag_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelHashtag.Text = comboBoxHashtag.Text.First().ToString().ToUpper()+ comboBoxHashtag.Text.Substring(1);

            List<HashtagEvolution> filteredList = evolution_dictionary[comboBoxTimeFrame.Text].Where(x => x.name.Equals(comboBoxHashtag.SelectedItem)).ToList();

            Refresh_Evolution_Charts(filteredList);
            FillSummaryLabels(filteredList);
            FillLastStatus(last_status_array);

        }

        private void ComboBoxTimeFrame_SelectedIndexChanged(object sender, EventArgs e)
        {
            labelTime.Text = comboBoxTimeFrame.Text;

            if (comboBoxHashtag.SelectedIndex == -1)
            {

                //llenamos combobox hashtag
                FillComboBoxHashtag(evolution_dictionary[comboBoxTimeFrame.Text]);
                comboBoxHashtag.SelectedItem = comboBoxHashtag.Items[0];
            }
            else
            {
                List<HashtagEvolution> filteredList = evolution_dictionary[comboBoxTimeFrame.Text].Where(x => x.name.Equals(comboBoxHashtag.SelectedItem)).ToList();

                Refresh_Evolution_Charts(filteredList);
                FillSummaryLabels(filteredList);
                FillLastStatus(last_status_array);
            }



        }

        private void Refresh_Evolution_Charts(List<HashtagEvolution> values)
        {
            FillStatusChart(values);
            FillUserChart(values);
            FillRetweetChart(values);
        }

        private void FillSummaryLabels(List<HashtagEvolution> result)
        {
            labelStatusResult.Text = FormatNumber(result[0].total_status);
            labelRetweetsResult.Text = FormatNumber(result[0].total_retweets);
            labelUsersResult.Text = FormatNumber(result[0].total_users);
            labelFavResult.Text = FormatNumber(result[0].total_favorite);

            if(result[0].name.Equals("europa"))
                pictureBox1.BackgroundImage= TwitterAnalytics.Properties.Resources.iconfinder_eu_53162;
            else if (result[0].name.Equals("catalunya"))
                pictureBox1.BackgroundImage = TwitterAnalytics.Properties.Resources.catalunya;
            else if (result[0].name.Equals("independencia"))
                pictureBox1.BackgroundImage = TwitterAnalytics.Properties.Resources.independence;
            else if (result[0].name.Equals("diada"))
                pictureBox1.BackgroundImage = TwitterAnalytics.Properties.Resources.diada;
            else if (result[0].name.Equals("barcelona"))
                pictureBox1.BackgroundImage = TwitterAnalytics.Properties.Resources.iconfinder_barcelona_171407;

        }

        private string FormatNumber(string number)
        {
            string output;
            if (Int64.Parse(number) > 1000000)
                output = ((float)(Int64.Parse(number)) / 1000000).ToString("0.00") + "M";
            else if (Int64.Parse(number) > 1000)
                output = ((float)(Int64.Parse(number)) / 1000).ToString("0.00") + "k";
            else
                output = number;

            return output;
        }

        private void FillLastStatus(List<HashtagLastStatus> statusList)
        {
            dataGridViewLastStatus.Rows.Clear();
            //dataGridViewLastStatus.Refresh();
            //dataGridViewLastStatus.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCells;
            dataGridViewLastStatus.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            dataGridViewLastStatus.RowHeadersWidth = 20;
            dataGridViewLastStatus.EnableHeadersVisualStyles = false;


            //dataGridViewLastStatus.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(155, 210, 255);

            dataGridViewLastStatus.RowsDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(218, 238, 255);
            dataGridViewLastStatus.AlternatingRowsDefaultCellStyle.BackColor = System.Drawing.Color.White;


            List<HashtagLastStatus> filteredstatusList = statusList.Where(x => x.name.Equals(comboBoxHashtag.SelectedItem)).ToList();

            foreach (HashtagLastStatus ls in filteredstatusList)
            {
                int n = dataGridViewLastStatus.Rows.Add();

                dataGridViewLastStatus.Rows[n].Cells[0].Value = ls.creation_datetime;
                dataGridViewLastStatus.Rows[n].Cells[1].Value = ls.user_location;
                dataGridViewLastStatus.Rows[n].Cells[2].Value = ls.link;
                dataGridViewLastStatus.Rows[n].Cells[3].Value = ls.text;

            }
            //dataGridViewLastStatus.ClearSelection();

        }
        private void DataGridViewLastStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewLastStatus.SelectedCells[0].ColumnIndex.Equals(3))
            {
                System.Diagnostics.Process.Start(dataGridViewLastStatus.SelectedCells[0].Value.ToString());
            }
            // Get the url from the row
            // var url = e.Row.Columns[1].Value;
            //     Process.Start(url);

        }

        private void ImgReturn_Click(object sender, EventArgs e)
        {
            gg.Show();
            this.Close();
        }

        private void ImgMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ImgClose_Click(object sender, EventArgs e)
        {
            gg.Show();
            this.Close();
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void LabelUsersResult_Click(object sender, EventArgs e)
        {

        }
    }
}
