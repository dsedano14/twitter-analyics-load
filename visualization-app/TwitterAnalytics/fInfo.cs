﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TwitterAnalytics
{
    public partial class fInfo : Form
    {
        public fInfo(String type, String text)
        {
            InitializeComponent();

            if (type.Equals("guide"))
            {
                guidesFill(text);
            }
            else if (type.Equals("about"))
            {
                guidesAbout(text);
            }
            else
            {
                guidesContact(text);
            }


        }

        private void guidesFill(String text)
        {
            pictureBox1.BackgroundImage = TwitterAnalytics.Properties.Resources.iconfinder_38_Target_Audience_1688838;
            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;

            pictureBox2.BackgroundImage = TwitterAnalytics.Properties.Resources.iconfinder_flame_3688488;
            pictureBox2.BackgroundImageLayout = ImageLayout.Stretch;

            pictureBox3.BackColor = Color.Transparent;
            pictureBox3.BackgroundImage = TwitterAnalytics.Properties.Resources.iconfinder_26_google_trends_topic_featured_service_2109149;
            pictureBox3.BackgroundImageLayout = ImageLayout.Stretch;


            string[] str_splitted = text.Split('|');

            Font bold = new Font(richTextBox1.Font, FontStyle.Bold);
            Font regular = new Font(richTextBox1.Font, FontStyle.Regular);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[0]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[1]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[2]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[3]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[4]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[5]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[6]);
        }

        private void guidesContact(String text)
        {
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;

            string[] str_splitted = text.Split('|');

            Font bold = new Font(richTextBox1.Font, FontStyle.Bold);
            Font regular = new Font(richTextBox1.Font, FontStyle.Regular);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[0]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[1]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[2]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[3]);


            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[4]);


            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[5]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[6]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[7]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[8]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[9]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[10]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[11]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[12]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[13]);



        }

        private void guidesAbout(String text)
        {
            pictureBox1.Visible = false;
            pictureBox2.Visible = false;
            pictureBox3.Visible = false;

            string[] str_splitted = text.Split('|');

            Font bold = new Font(richTextBox1.Font, FontStyle.Bold);
            Font regular = new Font(richTextBox1.Font, FontStyle.Regular);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[0]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[1]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[2]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[3]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[4]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[5]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[6]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[7]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[8]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[9]);

            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[10]);

            richTextBox1.SelectionFont = bold;
            richTextBox1.AppendText(str_splitted[11]);
            richTextBox1.SelectionFont = regular;
            richTextBox1.AppendText(str_splitted[12]);


        }
    }
}
