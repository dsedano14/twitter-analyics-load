﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Cloud.BigQuery.V2;

namespace TwitterAnalytics
{
    public partial class fMain : Form
    {

        public fMain()
        {
            InitializeComponent();
            this.Show();
            timer1.Start();


        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            btnUser.Left -= 15;
            btnHashtag.Left -= 15;
            btnTrend.Left -= 15;

            if (btnUser.Left <=135) {
                timer1.Stop();
                timer2.Start();
            }

        }


        private void Timer2_Tick(object sender, EventArgs e)
        {
            panel1.Top += 5;

            if (panel1.Top >= 0)
            {
                imgTopTitle.SizeMode = PictureBoxSizeMode.StretchImage;
                imgTopTitle.BackColor = Color.Transparent;


                imgTopTitle.Visible = true;
                labelTitle.Visible = true;
                imgMin.Visible = true;
                imgClose.Visible = true;
                panelLeft.Visible = true;
                labelGuide.Visible = true;
                labelAbout.Visible = true;
                labelContact.Visible = true;
                timer2.Stop();
            }
        }

        private void BtnUser_Click(object sender, EventArgs e)
        {
            /* LoadingBQ loading = new LoadingBQ(this, "SELECT b.user_name, b.user_desc, b.user_url, b.user_location, b.user_status_count, b.user_profile_img_url as img_url, ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_follower_count as STRING) order by a.inserted_date asc), ', ') as follower "
             +" , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_favorite_count as STRING) order by a.inserted_date asc), ', ') as favorite"
             +" , ARRAY_TO_STRING(ARRAY_AGG(CAST(a.user_friends_count as STRING) order by a.inserted_date asc), ', ') as friend"
             +" , ARRAY_TO_STRING(ARRAY_AGG(CAST(TIMESTAMP_TRUNC(a.inserted_date, HOUR) as STRING) order by a.inserted_date asc), ', ') as date  FROM `dsedano-datalake-twitter.ocean.user_progress` a"
             + " JOIN `dsedano-datalake-twitter.raw_last.user_twitter` b on a.user_id = b.user_id"
             + " where a.user_id in (80625624, 84053338)"
             + " group by 1,2,3,4,5,6"
             + " order by 1 desc");
             */
            Dictionary<string, string> userQueries = new Dictionary<string, string>();

            userQueries.Add("user_evolution", FillNames.user_evolution_query);
            userQueries.Add("user_last_status", FillNames.user_last_status_query);


            //String[] query = { FillNames.user_evolution_query, FillNames.user_last_status_query };
            LoadingBQ loading = new LoadingBQ(this, "user", userQueries);


            loading.Show();

            this.Hide();
        }

        private void BtnUser_MouseHover(object sender, EventArgs e)
        {
            PictureBox obj = sender as PictureBox;
            string name = obj.Name;
            ToolTip tt = new ToolTip();

            if (name.Equals("btnUser"))
            {
                tt.SetToolTip(btnUser, "User Analytics");
            }else if (name.Equals("btnHashtag"))
            {
                tt.SetToolTip(btnHashtag, "Hashtag Analytics");

            }
            else
            {
                tt.SetToolTip(btnTrend, "Trends Analytics");

            }

        }

        private void ImgMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ImgClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LabelGuide_Click(object sender, EventArgs e)
        {

            String text = "Click on the desired button to start analysing your data.| \n\n\n\n\tUser Analysis:| Account related metrics and its evolution during the timeline.|"
                + "\n\n\n\tHashtag Analysis:| Keyword related analysis, check evolution timeline\n\t\t\tand tweets linked to a certain topic.|"
                + "\n\n\n\tTrends Analysis:| Trending topics timeline evolution in serveral regions.";

            fInfo info = new fInfo("guide", text);
            DialogResult dr = info.ShowDialog(this);

        }

        private void LabelGuide_MouseHover(object sender, EventArgs e)
        {
            Label obj = sender as Label;
            string name = obj.Name;

            if (name.Equals("labelAbout")){
                labelAbout.BackColor = Color.FromArgb(29, 202, 255);
            }else if (name.Equals("labelContact")){
                labelContact.BackColor = Color.FromArgb(29, 202, 255);
            }
            else{
                labelGuide.BackColor = Color.FromArgb(29, 202, 255);
            }
        }

        private void LabelGuide_MouseLeave(object sender, EventArgs e)
        {
            Label obj = sender as Label;
            string name = obj.Name;

            if (name.Equals("labelAbout"))
            {
                labelAbout.BackColor = Color.FromArgb(192, 222, 237);
            }
            else if (name.Equals("labelContact"))
            {
                labelContact.BackColor = Color.FromArgb(192, 222, 237);
            }
            else
            {
                labelGuide.BackColor = Color.FromArgb(192, 222, 237);
            }
        }

        private void LabelAbout_Click(object sender, EventArgs e)
        {

            String text = "dsedano Twitter Analytics app| was built to give an easy access to Twitter relevant data.\n\nData showed in the app is near |Real Time| and is continuously |updating|.|"
                + "\n\nAll data topics analyzed are handled by |Google CLoud Platform.|\n\nIf you want to add more topic in the analysis, contact IT department and they will help you."
                + "\n\nThe aim of this app is to provide information to support you in the| decision making process| and become a |data driven| company.";

            fInfo info = new fInfo("about", text);
            DialogResult dr = info.ShowDialog(this);

        }


        private void LabelContact_Click(object sender, EventArgs e)
        {
  
            String text = "Contact information.| \n\nIf you have any doubts, questions or suggestion do not hesitate to contact support teams. Feedbacks received from end users are very important to keep improving the user experience.|" +
                "\n\n\nTechnical issues:| Contact| techsupport@freetwitteranalytics.com| to solve issues related to the app user experience.|"
                + "\n\nSuggestions:|  Contact |devteam@freetwitteranalytics.com| to suggest improvements to our developers and keep growing.|"
                + "\n\nSupport:| Contact |support@freetwitteranalytics.com| to solve doubts about the app.";

            fInfo info = new fInfo("contact", text);
            DialogResult dr = info.ShowDialog(this);

        }

        private void BtnTrend_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> trendQueries = new Dictionary<string, string>();

            trendQueries.Add("trend_top", FillNames.trend_top_10_query);
            trendQueries.Add("trend_repeated", FillNames.trend_top_repeated_month);
            //userQueries.Add("user_last_status", FillNames.user_last_status_query);


            //String[] query = { FillNames.user_evolution_query, FillNames.user_last_status_query };
            LoadingBQ loading = new LoadingBQ(this, "trend", trendQueries);


            loading.Show();

            this.Hide();
        }

        private void BtnHashtag_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> hashtagQueries = new Dictionary<string, string>();

            //poner nombres que quieran que salga en el selector
            hashtagQueries.Add("evolution_hour", FillNames.hashtag_evolution_last_hour);
            hashtagQueries.Add("evolution_day", FillNames.hashtag_evolution_last_day);
            hashtagQueries.Add("last_status", FillNames.hashtag_last_status);
            hashtagQueries.Add("evolution_week", FillNames.hashtag_evolution_last_week);
            hashtagQueries.Add("evolution_month", FillNames.hashtag_evolution_last_month);

            LoadingBQ loading_ht = new LoadingBQ(this, "hashtag", hashtagQueries);

            loading_ht.Show();

            this.Hide();

        }
    }
}
