﻿using Google.Cloud.BigQuery.V2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace TwitterAnalytics
{
    public partial class LoadingBQ : Form
    {
        private Dictionary<string, string> query;
        private string type;
        private fMain fmain;
        bigQuery bq = new bigQuery();

        public LoadingBQ(TwitterAnalytics.fMain mainf, String type, Dictionary<string, string> query)
        {
            InitializeComponent();

            //pictureBox1.Image = TwitterAnalytics.Properties.Resources.loading_cube_gif;
            pictureBox1.Image = TwitterAnalytics.Properties.Resources.twitterfly;


            this.query = query;
            this.type = type;
            this.fmain = mainf;

            Console.WriteLine(type);
            Console.WriteLine("type");

            BackgroundWorker backgroundWorker1 = new BackgroundWorker();

            backgroundWorker1.DoWork += backgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += backgroundWorker1_RunWorkerCompleted;
            backgroundWorker1.RunWorkerAsync();
            
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (type.Equals("user"))
            {
                e.Result = bq.Query(type, query);
            }
            else if (type.Equals("trend"))
            {
                e.Result = bq.QueryTrend(type, query);
            }
            else if (type.Equals("hashtag"))
            {
                e.Result = bq.QueryHashtag(type, query);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (type.Equals("user"))
            {
                UserAnalysis fuserAnalysis = new UserAnalysis(fmain, e.Result); // This is bad
                fuserAnalysis.Show();


            }else if (type.Equals("trend"))
            {
                fTTopic trendAnalysis = new fTTopic(fmain, e.Result); // This is bad
                trendAnalysis.Show();


            }else if (type.Equals("hashtag")){
                fHashtag hashtagAnalysis = new fHashtag(fmain, e.Result); // This is bad
                hashtagAnalysis.Show();
            }
            //backgroundWorker1.CancelAsync();
           
            this.Close();
        }

    }
}
