﻿using Google.Cloud.BigQuery.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterAnalytics.Model;

namespace TwitterAnalytics
{
    class bigQuery
    {
        //String query;
        //string projectId = "your-project-id";

        /*
        public List<UserEvolution> Query(String type, String[] query)
        {
           
            BigQueryClient client = BigQueryClient.Create("your-project-id");

            //BigQueryClient client = BigQueryClient.Create(projectId);
            //string query = query;
            System.Console.WriteLine("query"+query);
            BigQueryJob job = client.CreateQueryJob(
                sql: query,
                //sql: "SELECT user_friends_count FROM `your-project-id.ocean.user_progress` where user_id = 74453123",
                parameters: null,
                options: new QueryOptions { UseQueryCache = false });
            // Wait for the job to complete.
            job.PollUntilCompleted();
            // Display the results

            List<UserEvolution> resultado_array = new List<UserEvolution>();
            string aux1, aux2, aux3, aux4;
            int i = 0;
            foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
            {
                UserEvolution resultado = new UserEvolution();

                resultado.name = $"{row["user_name"]}";
                resultado.desc = $"{row["user_desc"]}";
                resultado.url= $"{row["user_url"]}";
                resultado.location = $"{row["user_location"]}";
                resultado.status_count = $"{row["user_status_count"]}";
                resultado.img_url = $"{row["img_url"]}";
                aux1 = $"{row["follower"]}";
                aux2 = $"{row["date"]}";
                aux3 = $"{row["friend"]}";
                aux4 = $"{row["favorite"]}";

                resultado.follower = aux1.Split(',');
                resultado.friend = aux3.Split(',');
                resultado.favorite = aux4.Split(',');
                resultado.date = aux2.Split(',');


                resultado_array.Add(resultado);
                i += 1;
            }
            return resultado_array;
           
        }
        */

        public User Query(String type, Dictionary<string, string> queries)
        {
            User resultado_array = new User();

            if (type.Equals("user"))
            {
                resultado_array = UserBQPetition(queries);
            }
            else if (type.Equals("trend"))
            {

            }

            return resultado_array;
        }
        public Trend QueryTrend(String type, Dictionary<string, string> queries)
        {
            //List<TrendTop> resultado_array = new List<TrendTop>();
            Trend resultado_array = new Trend();

            if (type.Equals("trend"))
            {
                resultado_array = TrendBQPetition(queries);
            }

            return resultado_array;
        }
        public Hashtag QueryHashtag(String type, Dictionary<string, string> queries)
        {
            Hashtag resultado_array = new Hashtag();

            if (type.Equals("hashtag"))
            {
                resultado_array = HashtagBQPetition(queries);
            }

            return resultado_array;
        }

        private Hashtag HashtagBQPetition(Dictionary<string, string> queries)
        {
            BigQueryClient client = BigQueryClient.Create("your-project-id");
            Hashtag hashtag_final = new Hashtag();
            List<HashtagLastStatus> hashtaglast_array = new List<HashtagLastStatus>();
            Dictionary<string, List<HashtagEvolution>> hashtagevol_hour_array_dic = new Dictionary<string, List<HashtagEvolution>>();
            //Dictionary<string, List<HashtagEvolution>> hashtagevol_day_array_dic = new Dictionary<string, List<HashtagEvolution>>();

            foreach (KeyValuePair<string, string> kvp in queries)
            {
                List<HashtagEvolution> hashtagevol_hour_array = new List<HashtagEvolution>();

                BigQueryJob job = client.CreateQueryJob(
                sql: kvp.Value,
                parameters: null,
                options: new QueryOptions { UseQueryCache = false });
                // Wait for the job to complete.
                job.PollUntilCompleted();

                if (kvp.Key.Equals("evolution_hour") || kvp.Key.Equals("evolution_day") || kvp.Key.Equals("evolution_week") || kvp.Key.Equals("evolution_month"))
                {
                    int i = 0;
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        HashtagEvolution resultado = new HashtagEvolution();

                        resultado.name = $"{row["hashtag_name"]}";
                        resultado.time = $"{row["time"]}";
                        resultado.num_status = $"{row["num_status"]}";
                        resultado.num_users = $"{row["num_users"]}";
                        resultado.num_retweets = $"{row["retweets"]}";
                        resultado.num_favorite = $"{row["favorite"]}";
                        resultado.total_status = $"{row["total_status"]}";
                        resultado.total_users = $"{row["total_users"]}";
                        resultado.total_retweets = $"{row["total_retweets"]}";
                        resultado.total_favorite = $"{row["total_fav"]}";

                        hashtagevol_hour_array.Add(resultado);
                        i += 1;
                    }

                    if (kvp.Key.Equals("evolution_hour"))   hashtagevol_hour_array_dic.Add("Last Hour Evolution", hashtagevol_hour_array);
                    else if( kvp.Key.Equals("evolution_day")) hashtagevol_hour_array_dic.Add("Last Day Evolution", hashtagevol_hour_array);
                    else if (kvp.Key.Equals("evolution_week")) hashtagevol_hour_array_dic.Add("Last Week Evolution", hashtagevol_hour_array);
                    else if (kvp.Key.Equals("evolution_month")) hashtagevol_hour_array_dic.Add("Last Month Evolution", hashtagevol_hour_array);


                    hashtag_final.hashtag_evolution = hashtagevol_hour_array_dic;
                }
                else if (kvp.Key.Equals("last_status"))
                { 
                    //añadir en el dictionary la asignacion de el nombre a devolver para el selector
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        HashtagLastStatus resultado = new HashtagLastStatus();

                        resultado.name = $"{row["hashtag_name"]}";
                        resultado.creation_datetime = $"{row["status_creation_datetime"]}";
                        resultado.text = $"{row["text"]}";
                        resultado.fav_count = $"{row["favorite_count"]}";
                        resultado.retweet_count = $"{row["status_retweet_count"]}";
                        resultado.user_location = $"{row["user_location"]}";
                        resultado.link = $"{row["status_link"]}";

                        hashtaglast_array.Add(resultado);
                    }
                    hashtag_final.last_status = hashtaglast_array;

                }

            }
            hashtag_final.hashtag_evolution = hashtagevol_hour_array_dic;
            return hashtag_final;

        }

        private Trend TrendBQPetition(Dictionary<string, string> queries)
        {
            BigQueryClient client = BigQueryClient.Create("your-project-id");
            Trend trend_final = new Trend();
            List<TrendTop> trend_array = new List<TrendTop>();
            List<TrendRepeated> trend_repeated = new List<TrendRepeated>();


            foreach (KeyValuePair<string, string> kvp in queries)
            {
                BigQueryJob job = client.CreateQueryJob(
                sql: kvp.Value,
                parameters: null,
                options: new QueryOptions { UseQueryCache = false });
                // Wait for the job to complete.
                job.PollUntilCompleted();

                if (kvp.Key.Equals("trend_top"))
                {
                    int i = 0;
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        TrendTop resultado = new TrendTop();

                        resultado.day = $"{row["day"]}";
                        resultado.place = $"{row["trend_place"]}";
                        resultado.name = $"{row["trend_name"]}";
                        resultado.query = $"{row["query"]}";
                        resultado.tweet_volume = $"{row["trend_tweet_volume"]}";

                        trend_array.Add(resultado);
                        i += 1;
                    }

                    trend_final.trends_array = trend_array;

                }
                else if (kvp.Key.Equals("trend_repeated"))
                {
                    int i = 0;
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        TrendRepeated resultado = new TrendRepeated();
                        
                        resultado.trend_place = $"{row["trend_place"]}";
                        resultado.trend_name = $"{row["trend_name"]}";
                        resultado.num_repeated = $"{row["num_repeated"]}";

                        trend_repeated.Add(resultado);
                        i += 1;
                    }
                    trend_final.trends_repeated = trend_repeated;

                }

            }
            return trend_final;

        }

        private User UserBQPetition(Dictionary<string, string> queries)
        {
            User user_final = new User();

            BigQueryClient client = BigQueryClient.Create("your-project-id");

            //BigQueryClient client = BigQueryClient.Create(projectId);
            //string query = query;
            //System.Console.WriteLine("query" + queries[0]);
            List<UserEvolution> resultado_array = new List<UserEvolution>();
            List<UserStatus> status_array = new List<UserStatus>();

            foreach (KeyValuePair<string, string> kvp in queries)
            {
                BigQueryJob job = client.CreateQueryJob(
                sql: kvp.Value,
                //sql: "SELECT user_friends_count FROM `your-project-id.ocean.user_progress` where user_id = 74453123",
                parameters: null,
                options: new QueryOptions { UseQueryCache = false });
                // Wait for the job to complete.
                job.PollUntilCompleted();

                if (kvp.Key.Equals("user_evolution"))
                {
                    string aux1, aux2, aux3, aux4;
                    int i = 0;
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        UserEvolution resultado = new UserEvolution();

                        resultado.name = $"{row["user_name"]}";
                        resultado.screenname = $"{row["user_screenname"]}";
                        resultado.desc = $"{row["user_desc"]}";
                        resultado.url = $"{row["user_url"]}";
                        resultado.location = $"{row["user_location"]}";
                        resultado.status_count = $"{row["user_status_count"]}";
                        resultado.img_url = $"{row["img_url"]}";
                        aux1 = $"{row["follower"]}";
                        aux2 = $"{row["date"]}";
                        aux3 = $"{row["friend"]}";
                        aux4 = $"{row["favorite"]}";

                        resultado.follower = aux1.Split(',');
                        resultado.friend = aux3.Split(',');
                        resultado.favorite = aux4.Split(',');
                        resultado.date = aux2.Split(',');


                        resultado_array.Add(resultado);
                        i += 1;
                    }

                }
                else
                {
                    foreach (BigQueryRow row in client.GetQueryResults(job.Reference))
                    {
                        UserStatus status = new UserStatus();

                        status.name = $"{row["user_name"]}";
                        status.text = $"{row["text"]}";
                        status.created_datetime = $"{row["status_creation_datetime"]}";
                        status.retweet_count = $"{row["status_retweet_count"]}";
                        status.fav_count = $"{row["favorite_count"]}";
                        status.status_link = $"{row["status_link"]}";


                        status_array.Add(status);
                    }

                }
            }

            // Display the results

            user_final.user_evolution = resultado_array;
            user_final.user_status = status_array;

            return user_final;

        }
    }
}
