﻿namespace TwitterAnalytics
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.labelTitle = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.labelContact = new System.Windows.Forms.Label();
            this.labelAbout = new System.Windows.Forms.Label();
            this.labelGuide = new System.Windows.Forms.Label();
            this.labelbtm = new System.Windows.Forms.Label();
            this.imgClose = new System.Windows.Forms.PictureBox();
            this.imgMin = new System.Windows.Forms.PictureBox();
            this.imgTopTitle = new System.Windows.Forms.PictureBox();
            this.btnTrend = new System.Windows.Forms.PictureBox();
            this.btnHashtag = new System.Windows.Forms.PictureBox();
            this.btnUser = new System.Windows.Forms.PictureBox();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTopTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTrend)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHashtag)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUser)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 2;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(132)))), ((int)(((byte)(180)))));
            this.panel1.Location = new System.Drawing.Point(0, -65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(542, 65);
            this.panel1.TabIndex = 5;
            // 
            // timer2
            // 
            this.timer2.Interval = 1;
            this.timer2.Tick += new System.EventHandler(this.Timer2_Tick);
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(132)))), ((int)(((byte)(180)))));
            this.labelTitle.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.ForeColor = System.Drawing.Color.White;
            this.labelTitle.Location = new System.Drawing.Point(69, 19);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(175, 29);
            this.labelTitle.TabIndex = 7;
            this.labelTitle.Text = "Twitter Analytics";
            this.labelTitle.Visible = false;
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(222)))), ((int)(((byte)(237)))));
            this.panelLeft.Controls.Add(this.labelContact);
            this.panelLeft.Controls.Add(this.labelAbout);
            this.panelLeft.Controls.Add(this.labelGuide);
            this.panelLeft.Location = new System.Drawing.Point(0, 65);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(83, 392);
            this.panelLeft.TabIndex = 10;
            this.panelLeft.Visible = false;
            // 
            // labelContact
            // 
            this.labelContact.AutoSize = true;
            this.labelContact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelContact.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContact.Location = new System.Drawing.Point(2, 77);
            this.labelContact.Name = "labelContact";
            this.labelContact.Size = new System.Drawing.Size(79, 19);
            this.labelContact.TabIndex = 0;
            this.labelContact.Text = "   Contact  ";
            this.labelContact.Visible = false;
            this.labelContact.Click += new System.EventHandler(this.LabelContact_Click);
            this.labelContact.MouseLeave += new System.EventHandler(this.LabelGuide_MouseLeave);
            this.labelContact.MouseHover += new System.EventHandler(this.LabelGuide_MouseHover);
            // 
            // labelAbout
            // 
            this.labelAbout.AutoSize = true;
            this.labelAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelAbout.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAbout.Location = new System.Drawing.Point(2, 49);
            this.labelAbout.Name = "labelAbout";
            this.labelAbout.Size = new System.Drawing.Size(79, 19);
            this.labelAbout.TabIndex = 0;
            this.labelAbout.Text = "   About     ";
            this.labelAbout.Visible = false;
            this.labelAbout.Click += new System.EventHandler(this.LabelAbout_Click);
            this.labelAbout.MouseLeave += new System.EventHandler(this.LabelGuide_MouseLeave);
            this.labelAbout.MouseHover += new System.EventHandler(this.LabelGuide_MouseHover);
            // 
            // labelGuide
            // 
            this.labelGuide.AutoSize = true;
            this.labelGuide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelGuide.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGuide.Location = new System.Drawing.Point(2, 20);
            this.labelGuide.Name = "labelGuide";
            this.labelGuide.Size = new System.Drawing.Size(79, 19);
            this.labelGuide.TabIndex = 0;
            this.labelGuide.Text = "   Guide     ";
            this.labelGuide.Visible = false;
            this.labelGuide.Click += new System.EventHandler(this.LabelGuide_Click);
            this.labelGuide.MouseLeave += new System.EventHandler(this.LabelGuide_MouseLeave);
            this.labelGuide.MouseHover += new System.EventHandler(this.LabelGuide_MouseHover);
            // 
            // labelbtm
            // 
            this.labelbtm.AutoSize = true;
            this.labelbtm.Font = new System.Drawing.Font("Calibri", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelbtm.Location = new System.Drawing.Point(432, 430);
            this.labelbtm.Name = "labelbtm";
            this.labelbtm.Size = new System.Drawing.Size(105, 11);
            this.labelbtm.TabIndex = 11;
            this.labelbtm.Text = "@dsedano twitter analytics";
            // 
            // imgClose
            // 
            this.imgClose.BackgroundImage = global::TwitterAnalytics.Properties.Resources._296812_48;
            this.imgClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgClose.Location = new System.Drawing.Point(516, 4);
            this.imgClose.Name = "imgClose";
            this.imgClose.Size = new System.Drawing.Size(20, 20);
            this.imgClose.TabIndex = 9;
            this.imgClose.TabStop = false;
            this.imgClose.Visible = false;
            this.imgClose.Click += new System.EventHandler(this.ImgClose_Click);
            // 
            // imgMin
            // 
            this.imgMin.BackgroundImage = global::TwitterAnalytics.Properties.Resources._3838428_48;
            this.imgMin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgMin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgMin.Location = new System.Drawing.Point(489, 5);
            this.imgMin.Name = "imgMin";
            this.imgMin.Size = new System.Drawing.Size(20, 20);
            this.imgMin.TabIndex = 8;
            this.imgMin.TabStop = false;
            this.imgMin.Visible = false;
            this.imgMin.Click += new System.EventHandler(this.ImgMin_Click);
            // 
            // imgTopTitle
            // 
            this.imgTopTitle.BackColor = System.Drawing.Color.Transparent;
            this.imgTopTitle.BackgroundImage = global::TwitterAnalytics.Properties.Resources._4518796_64_white;
            this.imgTopTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgTopTitle.Location = new System.Drawing.Point(12, 12);
            this.imgTopTitle.Name = "imgTopTitle";
            this.imgTopTitle.Size = new System.Drawing.Size(40, 40);
            this.imgTopTitle.TabIndex = 6;
            this.imgTopTitle.TabStop = false;
            this.imgTopTitle.Visible = false;
            // 
            // btnTrend
            // 
            this.btnTrend.BackgroundImage = global::TwitterAnalytics.Properties.Resources.iconfinder_flame_3688488;
            this.btnTrend.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTrend.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTrend.Location = new System.Drawing.Point(840, 188);
            this.btnTrend.Name = "btnTrend";
            this.btnTrend.Size = new System.Drawing.Size(101, 93);
            this.btnTrend.TabIndex = 4;
            this.btnTrend.TabStop = false;
            this.btnTrend.Click += new System.EventHandler(this.BtnTrend_Click);
            this.btnTrend.MouseHover += new System.EventHandler(this.BtnUser_MouseHover);
            // 
            // btnHashtag
            // 
            this.btnHashtag.BackgroundImage = global::TwitterAnalytics.Properties.Resources.iconfinder_26_google_trends_topic_featured_service_2109149;
            this.btnHashtag.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnHashtag.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHashtag.Location = new System.Drawing.Point(704, 188);
            this.btnHashtag.Name = "btnHashtag";
            this.btnHashtag.Size = new System.Drawing.Size(101, 93);
            this.btnHashtag.TabIndex = 3;
            this.btnHashtag.TabStop = false;
            this.btnHashtag.Click += new System.EventHandler(this.BtnHashtag_Click);
            this.btnHashtag.MouseHover += new System.EventHandler(this.BtnUser_MouseHover);
            // 
            // btnUser
            // 
            this.btnUser.BackgroundImage = global::TwitterAnalytics.Properties.Resources.iconfinder_38_Target_Audience_1688838;
            this.btnUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUser.Location = new System.Drawing.Point(573, 188);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(101, 93);
            this.btnUser.TabIndex = 2;
            this.btnUser.TabStop = false;
            this.btnUser.Click += new System.EventHandler(this.BtnUser_Click);
            this.btnUser.MouseHover += new System.EventHandler(this.BtnUser_MouseHover);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(540, 450);
            this.Controls.Add(this.labelbtm);
            this.Controls.Add(this.panelLeft);
            this.Controls.Add(this.imgClose);
            this.Controls.Add(this.imgMin);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.imgTopTitle);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnTrend);
            this.Controls.Add(this.btnHashtag);
            this.Controls.Add(this.btnUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgTopTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTrend)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnHashtag)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUser)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox btnUser;
        private System.Windows.Forms.PictureBox btnHashtag;
        private System.Windows.Forms.PictureBox btnTrend;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.PictureBox imgTopTitle;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.PictureBox imgMin;
        private System.Windows.Forms.PictureBox imgClose;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Label labelbtm;
        private System.Windows.Forms.Label labelGuide;
        private System.Windows.Forms.Label labelContact;
        private System.Windows.Forms.Label labelAbout;
    }
}

