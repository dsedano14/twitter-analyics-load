package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TopicEntityList implements Serializable {

    @JsonProperty
    private List<TopicEntity> EntityList= new ArrayList<TopicEntity>();



    public List<TopicEntity> getEntityList() {
        try{
            return EntityList;
        }catch(Exception e){
            throw e;
        }
    }

    public void AddEntity(TopicEntity entity){
        EntityList.add(entity);
    }
}
