package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class HashtagEntity  implements Serializable {
    @JsonProperty
    private Long status_id;
    @JsonProperty
    private String hashtag_name;
    @JsonProperty
    private Long inserted_date;



    public Long getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Long status_id) {
        this.status_id = status_id;
    }

    public String getHashtag_name() {
        return hashtag_name;
    }

    public void setHashtag_name(String hashtag_name) {
        this.hashtag_name = hashtag_name;
    }

    public Long getInserted_date() {
        return inserted_date;
    }

    public void setInserted_date(Long inserted_date) {
        this.inserted_date = inserted_date;
    }
}
