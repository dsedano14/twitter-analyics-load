package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.api.services.bigquery.model.TableSchema;

import java.io.Serializable;

public class TopicEntity implements Serializable{


    @JsonProperty
    private String subscription;
    @JsonProperty
    private String name;
    @JsonProperty
    private String table;
    @JsonProperty
    private TableSchema tableschema;

    public String getSubscription() {
        return subscription;
    }

    public void setSubscription(String topic) {
        this.subscription = topic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public TableSchema getTableschema() {
        return tableschema;
    }

    public void setTableschema(TableSchema tableschema) {
        this.tableschema = tableschema;
    }

    public TopicEntity(String subscription, String name, String table, TableSchema tableschema) {
        this.subscription = subscription;
        this.name = name;
        this.table = table;
        this.tableschema = tableschema;
    }
}
