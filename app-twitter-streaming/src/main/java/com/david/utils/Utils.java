package com.david.utils;


import com.david.model.HashtagEntity;
import com.david.model.TopicEntity;
import com.david.model.TopicEntityList;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.runners.dataflow.options.DataflowPipelineWorkerPoolOptions;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.transforms.DoFn;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.time.Instant;
import java.util.*;

import static com.david.utils.FillNames.*;
import static java.lang.Math.abs;


public class Utils {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Utils.class);


    public static CustomOptions setOptions(CustomOptions options, String environment, String type){
        options.setAppName(environment+FillNames.APPNAME);
        //options.setJobName(type.equals("batch") ? environment+FillNames.JOBNAME : environment+ JOBNAME_STREAM);
        options.setJobName(environment+ JOBNAME_STREAM);
        options.setStagingLocation(FillNames.STAGINGLOCATION);
        options.setProject(FillNames.PROJECT);
        options.setNetwork(FillNames.NETWORK);
        options.setSubnetwork(FillNames.SUBNETWORK);
        options.setRegion(FillNames.REGION);
        options.setZone(FillNames.ZONE);
        options.setTempLocation(FillNames.TEMPLOCATION);
        //options.setTempLocation("gs://app-twitter-temp/temp-loc");
        options.setGcpTempLocation(FillNames.GCPTEMPLOCATION);
        options.setAutoscalingAlgorithm(DataflowPipelineWorkerPoolOptions.AutoscalingAlgorithmType.THROUGHPUT_BASED);
        options.setMaxNumWorkers(1);
        options.setRunner(DataflowRunner.class);

        return options;
    }

    public interface CustomOptions extends DataflowPipelineOptions {
        @Description("Custom options")
        String getEnvironment();
        void setEnvironment(String environment);
    }



    public static Long setUTCToUnixTimestamp(Date o){
        Long time = o.getTime()/1000;
        return time;
    }

    public static boolean checkIfExistsPlace(Status status){
        Boolean exist=null;
        try{
            Status.class.getMethod(status.getPlace().getCountry());
            exist=true;
        }catch(Exception e){
            exist=false;
        }
        return exist;
    }

    public static boolean checkIfExistsQuoted(Status status){
        Boolean exist=null;
        Long obj1 = new Long(1);
        Long quotedStatusId =abs(status.getQuotedStatusId());

        if(!quotedStatusId.equals(obj1)) {
            exist = true;
            //System.out.println("QUOTEEEEEEEEEED" + status.getQuotedStatus().getText());
        }else{
            exist=false;
            //System.out.println("NOOOOOOOOTT  QUOTEEEEEEEEEED");
        }

        return exist;
    }

    public static Long getCurrentTimestamp(){
        Instant instant = Instant.now();
        long timeStampSeconds = instant.getEpochSecond();
        return timeStampSeconds;
    }



    public static class processPubSubData_new extends DoFn<String, TableRow> {

        @ProcessElement
        public void processElement(ProcessContext c) {

            String rowJson = c.element();


            try{
                System.out.println("--------------process---------------");
                Status status = TwitterObjectFactory.createStatus(rowJson);

                TableRow row = new TableRow();

                row.set(FillNames.STATUS_CREATED_AT,setUTCToUnixTimestamp(status.getCreatedAt()));
                row.set(FillNames.STATUS_ID,status.getId());
                row.set(FillNames.STATUS_TEXT,status.getText());
                row.set(FillNames.REPLIED_STATUS_ID,status.getInReplyToStatusId());
                row.set(FillNames.REPLIED_USER_ID,status.getInReplyToUserId());
                row.set(FillNames.ISFAVORITE,status.isFavorited());
                row.set(FillNames.ISRETEWEETED,status.isRetweeted());

                //row.set(FillNames.FAVORITE_COUNT, Integer.parseInt(fav1[0]));
                row.set(FillNames.FAVORITE_COUNT,status.getFavoriteCount());
                row.set(STATUS_RETWEET_COUNT,status.getRetweetCount());
                row.set(FillNames.REPLIED_SCREEN_NAME,status.getInReplyToScreenName());

                //row.set(FillNames.STATUS_GEOLOCATION_LATITUDE,status.getGeoLocation().getLatitude());
                //row.set(FillNames.STATUS_GEOLOCATION_LONGITUDE,status.getGeoLocation().getLongitude());
                //if(status.getUser().isGeoEnabled()){

                if(checkIfExistsPlace(status)){
                    row.set(FillNames.STATUS_PLACE_COUNTRY,status.getPlace().getCountry());
                    row.set(FillNames.STATUS_PLACE_FULLNAME,status.getPlace().getFullName());
                    row.set(FillNames.STATUS_PLACE_NAME,status.getPlace().getName());
                    row.set(FillNames.STATUS_PLACE_TYPE,status.getPlace().getPlaceType());
                    row.set(FillNames.STATUS_PLACE_ADDRESS,status.getPlace().getStreetAddress());
                    row.set(FillNames.STATUS_PLACE_URL,status.getPlace().getURL());
                }else{
                    row.set(FillNames.STATUS_PLACE_COUNTRY,"not geoenabled");
                    row.set(FillNames.STATUS_PLACE_FULLNAME,"not geoenabled");
                    row.set(FillNames.STATUS_PLACE_NAME,"not geoenabled");
                    row.set(FillNames.STATUS_PLACE_TYPE,"not geoenabled");
                    row.set(FillNames.STATUS_PLACE_ADDRESS,"not geoenabled");
                    row.set(FillNames.STATUS_PLACE_URL,"not geoenabled");
                }



                row.set(FillNames.IS_SENSITIVE,status.isPossiblySensitive());
                row.set(FillNames.STATUS_LANG,status.getLang());
                //row.set(FillNames.STATUS_CONTRIBUTORS,status.getContributors());
                if(status.isRetweet()){
                    row.set(FillNames.RETWEETED_STATUS_TEXT,status.getRetweetedStatus().getText());
                    row.set(FillNames.RETWEETED_STATUS_USER_NAME,status.getRetweetedStatus().getUser().getName());
                    row.set(FillNames.RETWEETED_STATUS_FAV_COUNT,status.getRetweetedStatus().getFavoriteCount());
                    row.set(FillNames.RETWEETED_STATUS_RET_COUNT,status.getRetweetedStatus().getRetweetCount());
                    row.set(FillNames.RETWEETED_STATUS_ID,status.getRetweetedStatus().getId());
                    row.set(RETWEETED_STATUS_USER_SCREEN_NAME,status.getRetweetedStatus().getUser().getScreenName());
                }else{
                    row.set(FillNames.RETWEETED_STATUS_TEXT,"Not Retweeted");
                    row.set(FillNames.RETWEETED_STATUS_USER_NAME,"Not Retweeted");
                    row.set(FillNames.RETWEETED_STATUS_FAV_COUNT,0);
                    row.set(FillNames.RETWEETED_STATUS_RET_COUNT,0);
                    row.set(RETWEETED_STATUS_ID,0);
                    row.set(RETWEETED_STATUS_USER_SCREEN_NAME,"Not Retweeted");
                }

                //row.set(FillNames.HASHTAG_ENTITY,status.getHashtagEntities().getText());
                row.set(FillNames.USER_ID,status.getUser().getId());
                row.set(FillNames.USER_NAME,status.getUser().getName());
                row.set(FillNames.USER_EMAIL,status.getUser().getEmail());
                row.set(FillNames.USER_SCREENNAME,status.getUser().getScreenName());
                row.set(FillNames.USER_LOCATION,status.getUser().getLocation());
                row.set(FillNames.USER_DESC,status.getUser().getDescription());
                row.set(FillNames.USER_PROFILE_IMG_URL,status.getUser().getProfileImageURL());
                row.set(FillNames.USER_PROFILE_IMG_URL_HTTPS,status.getUser().getProfileImageURLHttps());
                row.set(FillNames.USER_URL,status.getUser().getURL());
                row.set(FillNames.USER_FOLLOWER_COUNT,status.getUser().getFollowersCount());
                //row.set(FillNames.USER_STATUS,status.getUser().getStatus().getText());

                row.set(USER_ORIG_PROFILE_IMG_URL,status.getUser().getOriginalProfileImageURL());
                row.set(USER_BIG_PROFILE_IMG_URL,status.getUser().getBiggerProfileImageURL());
                row.set(USER_MINI_PROFILE_IMG_URL,status.getUser().getMiniProfileImageURL());
                row.set(USER_PROF_BACK_IMG_URL,status.getUser().getProfileBackgroundImageURL());
                row.set(USER_PROF_BACK_COLOR,status.getUser().getProfileBackgroundColor());
                row.set(FillNames.USER_FRIENDS_COUNT,status.getUser().getFriendsCount());
                row.set(FillNames.USER_CREATION_DATETIME,setUTCToUnixTimestamp(status.getUser().getCreatedAt()));
                row.set(FillNames.USER_FAV_COUNT,status.getUser().getFavouritesCount());
                row.set(FillNames.USER_LANG,status.getUser().getLang());
                row.set(FillNames.USER_STATUS_COUNT,status.getUser().getStatusesCount());
                row.set(FillNames.USER_IS_VERIFIED,status.getUser().isVerified());
                row.set(FillNames.USER_PROF_BANNER_300x100,status.getUser().getProfileBanner300x100URL());
                row.set(FillNames.USER_PROF_BANNER_600x200,status.getUser().getProfileBanner600x200URL());
                row.set(FillNames.USER_PROF_IMG_400x400,status.getUser().get400x400ProfileImageURL());
                row.set(USER_LISTED_COUNT,status.getUser().getListedCount());

                if(checkIfExistsQuoted(status)) {

                    row.set(STATUS_QUOTED, status.getQuotedStatus().getText());
                    row.set(STATUS_QUOTED_ID, status.getQuotedStatus().getId());
                    row.set(STATUS_QUOTED_SCREEN_NAME, status.getQuotedStatus().getUser().getScreenName());
                }else{
                    row.set(STATUS_QUOTED, "Not Quoted");
                    row.set(STATUS_QUOTED_ID, 0);
                    row.set(STATUS_QUOTED_SCREEN_NAME, "Not Quoted");
                }
                row.set("inserted_date",getCurrentTimestamp());

                c.output(row);
            }catch (Exception e){
                LOGGER.info(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public static TableSchema getValidationSchema_new() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_CREATED_AT).setType("TIMESTAMP"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_TEXT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.REPLIED_STATUS_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.REPLIED_USER_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.ISFAVORITE).setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName(FillNames.ISRETEWEETED).setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName(FillNames.FAVORITE_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_RETWEET_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.REPLIED_SCREEN_NAME).setType("STRING"));
        //fields.add(new TableFieldSchema().setName(FillNames.STATUS_GEOLOCATION_LATITUDE).setType("NUMERIC"));
        //fields.add(new TableFieldSchema().setName(FillNames.STATUS_GEOLOCATION_LONGITUDE).setType("NUMERIC"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_COUNTRY).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_FULLNAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_TYPE).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_ADDRESS).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_PLACE_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.IS_SENSITIVE).setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_LANG).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.STATUS_CONTRIBUTORS).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_TEXT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_USER_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_FAV_COUNT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_RET_COUNT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.RETWEETED_STATUS_USER_SCREEN_NAME).setType("STRING"));
        //fields.add(new TableFieldSchema().setName(FillNames.HASHTAG_ENTITY).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_EMAIL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_SCREENNAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_LOCATION).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_DESC).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROFILE_IMG_URL_HTTPS).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_FOLLOWER_COUNT).setType("INTEGER"));
        //fields.add(new TableFieldSchema().setName(FillNames.USER_STATUS).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_ORIG_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_BIG_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_MINI_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROF_BACK_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROF_BACK_COLOR).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_FRIENDS_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_CREATION_DATETIME).setType("TIMESTAMP"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_FAV_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_LANG).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_STATUS_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_IS_VERIFIED).setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROF_BANNER_300x100).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROF_BANNER_600x200).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_PROF_IMG_400x400).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.USER_LISTED_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED).setType("STRING"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED_SCREEN_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName("inserted_date").setType("TIMESTAMP"));

        TableSchema schema = new TableSchema().setFields(fields);

        return schema;
    }

    public static TableSchema getValidationSchemaHash() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName("status_id").setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("hashtag_name").setType("STRING"));
        fields.add(new TableFieldSchema().setName("inserted_date").setType("TIMESTAMP"));
        TableSchema schema = new TableSchema().setFields(fields);

        return schema;
    }

    public static class processPubSubDataHash extends DoFn<String, TableRow> {

        @ProcessElement
        public void processElement(ProcessContext c) {

            String rowJson = c.element();



            try{
                System.out.println("--------------processHash---------------");
                HashtagEntity status = new ObjectMapper().readValue(rowJson, HashtagEntity.class);

                TableRow row = new TableRow();
                row.set("status_id",status.getStatus_id());
                row.set("hashtag_name",status.getHashtag_name());
                row.set("inserted_date",status.getInserted_date());

                c.output(row);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static ConfigurationBuilder SetOptionsTwitter(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true);
        cb.setOAuthConsumerKey(_consumerKey);
        cb.setOAuthConsumerSecret(_consumerSecret);
        cb.setOAuthAccessToken(_accessToken);
        cb.setOAuthAccessTokenSecret(_ACCESSTOKENSECRET);
        cb.setJSONStoreEnabled(true);
        cb.setIncludeEntitiesEnabled(true);

        return cb;
    }

    public static TopicEntityList InitTopicList(){

        TopicEntityList entitylist = new TopicEntityList();

        entitylist.AddEntity(new TopicEntity(SUBSCRIPTION_STATUS, TOPIC_STATUS, PRO_TABLE, getValidationSchema_new()));
        entitylist.AddEntity(new TopicEntity(SUBSCRIPTION_HASHTAG, TOPIC_HASHTAG, PRO_TABLE_HASHTAG, getValidationSchemaHash()));

        return entitylist;
    }
}
