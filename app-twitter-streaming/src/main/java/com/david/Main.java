package com.david;


import com.david.model.*;
import com.david.utils.Utils;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.pubsub.PubsubIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Flatten;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionList;

import java.io.IOException;
import java.util.ArrayList;
import static com.david.utils.Utils.*;

import com.david.publisher.TwitterPublisher;

public class Main {

    public static void main(String[] args) throws IOException {


        TwitterPublisher.startPublisher();
        TwitterPublisher.RunTwitterStream();
/*
        //declare pipeline
        Pipeline pipeline_stream;
        Utils.CustomOptions options_stream = PipelineOptionsFactory.fromArgs(args).as(Utils.CustomOptions.class);
        String environment = "pro";
        setOptions(options_stream, environment, "stream");

        pipeline_stream = Pipeline.create(options_stream);

        ArrayList<PCollection<String>> raw = new ArrayList<>();

        TopicEntityList entity_list = InitTopicList();

        for(TopicEntity entity_topic :  entity_list.getEntityList()) {
            PCollection<String> pcol_stream = pipeline_stream.apply(entity_topic.getName()+"GetPubSubMessages", PubsubIO.readStrings().fromSubscription(entity_topic.getSubscription()));
            pcol_stream
                    .apply(entity_topic.getName()+"ProcessMessages", entity_topic.getName().equals("status") ? ParDo.of(new Utils.processPubSubData_new()) : ParDo.of(new Utils.processPubSubDataHash()))
                    .apply(entity_topic.getName()+"WriteMessages", BigQueryIO.writeTableRows().to(entity_topic.getTable())
                            .withSchema(entity_topic.getTableschema())
                            .withMethod(BigQueryIO.Write.Method.STREAMING_INSERTS)
                            .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                            .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

            raw.add(pcol_stream
            );
        }

        PCollectionList
                .of(raw).apply("Flatten services", Flatten.<String>pCollections());

        pipeline_stream.run();

*/

    }
}
