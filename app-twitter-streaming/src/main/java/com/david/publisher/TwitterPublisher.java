package com.david.publisher;

import com.david.config.keywordConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.david.utils.FillNames;
import com.google.api.core.ApiFuture;
import com.google.api.core.ApiFutures;
import com.google.cloud.pubsub.v1.Publisher;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.ProjectTopicName;
import com.google.pubsub.v1.PubsubMessage;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.david.utils.Utils.*;

public class TwitterPublisher{

    private static org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(TwitterPublisher.class);


    static int counter = 0;
    static int batchSize;
    static String gcpProjectId;
    static String topicId;
    static String topicId_Hasht;
    static ProjectTopicName topicName;
    static ProjectTopicName topicNameHash;
    static Publisher publisher;
    static Publisher publisher_hash;
    static List<ApiFuture<String>> futures = new ArrayList<>();
    static List<ApiFuture<String>> futures_hasht = new ArrayList<>();

    public static void startPublisher() throws IOException {

        batchSize = 10;
        gcpProjectId = FillNames.PROJECT;
        topicId = "testtopic";
        topicId_Hasht = "topic_hashtags";
        topicName = ProjectTopicName.of(gcpProjectId, topicId);
        topicNameHash = ProjectTopicName.of(gcpProjectId, topicId_Hasht);
        publisher = Publisher.newBuilder(topicName).build();
        publisher_hash = Publisher.newBuilder(topicNameHash).build();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    LOGGER.info("Thread to shut down the publisher.");

                    // Wait on any pending requests
                    ApiFutures.allAsList(futures).get();
                    publisher.shutdown();
                    ApiFutures.allAsList(futures_hasht).get();
                    publisher_hash.shutdown();
                    LOGGER.info("its been closed");

                } catch (Exception e) {
                    LOGGER.info("shutdowned error");

                    System.out.println("Error encountered while shutting down: " + e.getMessage());
                }
            }
        });

    }

    //ESTO deberia ir a Utils
    public static keywordConfig GetKeyWordsYAML(){

        Storage storage = StorageOptions.getDefaultInstance().getService();
        keywordConfig entitiesFromYAML = null;
        Blob blob = storage.get(BlobId.of("app-twitter-temp", "temp-loc/keywords.yaml"));

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        try {
            entitiesFromYAML = mapper.readValue(blob.getContent(), keywordConfig.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return entitiesFromYAML;
    }

    public static String[] GetHashtags(){

        keywordConfig entitiesFromYAML = GetKeyWordsYAML();

        List<String> key= new ArrayList<>();

        entitiesFromYAML.getKeyWord().stream()
                .filter(s->s.getType().contentEquals("hashtag"))
                .forEach(a -> key.add(a.getName()));

        String keywords[]=new String[key.size()];
        System.out.println(key.size());

        for (int i =0; i < key.size(); i++){
            keywords[i] = key.get(i);
            System.out.println(keywords);
        }

        return keywords;
    }

    public static String getHashtagFromStatus(Status status, String[] keywords){
        System.out.println("statusIDquoted");
        System.out.println(status.getQuotedStatusId());

        String statushashtag=null;
        for (String hashtag: keywords) {
            if(status.getText().toLowerCase().contains(hashtag.toLowerCase())){
                //System.out.println(hashtag);
                statushashtag=hashtag;
            }else if(status.isRetweet() && status.getRetweetedStatus().getText().toLowerCase().contains(hashtag.toLowerCase())){
                //System.out.println("retweeted"+status.isRetweet());
                statushashtag=hashtag;
            }else if(checkIfExistsQuoted(status) && status.getQuotedStatus().getText().toLowerCase().contains(hashtag.toLowerCase())) {
                //System.out.println("retweeted" + status.isRetweet());
                statushashtag = hashtag;
            }

        }
        return statushashtag;
    }

    public static void RunTwitterStream() {

        ConfigurationBuilder cb = SetOptionsTwitter();
        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
        FilterQuery fq = new FilterQuery();

        String keywords[]= GetHashtags();

        StatusListener listener = new StatusListener() {
            int tw=0;
            public void onStatus(Status status) {

                System.out.println("--------------status"+tw+"---------------");
                System.out.println(status.toString());


                String message = TwitterObjectFactory.getRawJSON(status);
                /*
                System.out.println("creation date:"+status.getCreatedAt());
                System.out.println("user creation:"+status.getUser().getCreatedAt());
                System.out.println("user epoch:"+status.getUser().getCreatedAt().getTime());
                System.out.println("user epoch transformed:"+setUTCToUnixTimestamp(status.getUser().getCreatedAt()));
*/
                //System.out.println("message:"+message);
            /*    String statusss =  status.toString();
                String fav[] = statusss.split("favoriteCount=");
                String fav1[] = fav[1].split(",");
*/
                String hashtag_status = getHashtagFromStatus(status, keywords);

                long timeStampSeconds =  getCurrentTimestamp();

                //HAY QUE COGER EL QUOTED STATUS PORQUE SE PUEDE REFERIR A ESE Y NO ENCUNETRAS EL HASHATG COÑO
                //status.getQuotedStatus().getText();
                //METER EL CAMPO ID COMO INTEGER

                try {
                    String stringToParse= "{\"status_id\":\""+status.getId()+"\",\"hashtag_name\":\""+hashtag_status+"\",\"inserted_date\":\""+timeStampSeconds+"\"}";
                    //System.out.println(stringToParse);
                    ByteString data_hasht = ByteString.copyFromUtf8(stringToParse);
                    PubsubMessage pubsubMessageHash = PubsubMessage.newBuilder().setData(data_hasht).build();
                    ApiFuture<String> future_hasht = publisher_hash.publish(pubsubMessageHash);
                    futures_hasht.add(future_hasht);

                    // Convert message to bytes
                    ByteString data = ByteString.copyFromUtf8(message);
                    PubsubMessage pubsubMessage = PubsubMessage.newBuilder().setData(data).build();

                    // Schedule a message to be published. Messages are automatically batched.
                    ApiFuture<String> future = publisher.publish(pubsubMessage);
                    futures.add(future);
                    System.out.println("añadido");
                    tw++;

                } catch (Exception e) {
                    LOGGER.info("shutdowned error");

                    System.out.println("Error encountered while shutting down: " + e.getMessage());
                }


            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
            }

            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
            }

            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {

            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };

        fq.track(keywords);
        String [] lang = {"en", "es"};
        fq.language(lang);
        twitterStream.addListener(listener);
        twitterStream.filter(fq);
    }

}
