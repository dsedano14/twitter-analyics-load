package com.david;
import com.david.model.*;
import com.david.utils.Utils;
import com.google.api.services.bigquery.model.TableSchema;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.io.IOException;

import static com.david.publisher.TwitterTrends.GetTwitterTrends;
import static com.david.publisher.TwitterUser.GetTwitterUsers;
import static com.david.utils.FillNames.*;
import static com.david.utils.Utils.*;

public class Main {

    public static void main(String[] args) throws IOException {


        ConfigurationBuilder cb = SetOptionsTwitter();
        Twitter twitter = new TwitterFactory(cb.build()).getInstance();


        //CAMBIAR NOMBRES PIPELINE:
        Pipeline pipeline_batch;
        Utils.CustomOptions options = PipelineOptionsFactory.fromArgs(args).as(Utils.CustomOptions.class);
        String environment = "pro";
        setOptions(options, environment, "batch");
        pipeline_batch = Pipeline.create(options);

        TableSchema tableSchemaTrend = Utils.getValidationSchemaTrends();
        TableSchema tableSchemaUser = Utils.getValidationSchemaUsers();


        EntityList entitylistBatch = new EntityList();


        entitylistBatch.AddEntity(new EntityTopic("", "user", PRO_TABLE_USER, tableSchemaUser));
        entitylistBatch.AddEntity(new EntityTopic("", "trend", PRO_TABLE_TREND, tableSchemaTrend));

    //coleccion de users
        PCollection<User> batch_user = pipeline_batch.apply("Reader User", Create.of(GetTwitterUsers(twitter)));
        batch_user.apply("Process User", ParDo.of(new Utils.processTweeterUsers()))
                // .apply("Writer", BigQueryIO.writeTableRows().to("pro".equals(environment) ? PRO_TABLE : DEVELOP_TABLE)
                .apply("Writer User", BigQueryIO.writeTableRows().to(PRO_TABLE_USER)
                        .withSchema(tableSchemaUser)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

    //collection de trends
        PCollection<TweetTrend> batch_trend = pipeline_batch.apply("Reader Trend", Create.of(GetTwitterTrends(twitter).getTweetTrendList()));
        batch_trend.apply("Process Trend", ParDo.of(new Utils.processTweeterTrends()))
                // .apply("Writer", BigQueryIO.writeTableRows().to("pro".equals(environment) ? PRO_TABLE : DEVELOP_TABLE)
                .apply("Writer Trend", BigQueryIO.writeTableRows().to(PRO_TABLE_TREND)
                        .withSchema(tableSchemaTrend)
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND));

        System.out.println("pipeline start");

        pipeline_batch.run();

    }
}
