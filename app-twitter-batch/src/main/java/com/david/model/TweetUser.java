package com.david.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class TweetUser implements Serializable{

    @JsonProperty
    private Long id;
    @JsonProperty
    private Long creation_datetime;
    @JsonProperty
    private Integer friends_count;
    @JsonProperty
    private Integer follower_count;
    @JsonProperty
    private Integer fav_count;
    @JsonProperty
    private String name;
    @JsonProperty
    private String language;
    @JsonProperty
    private String email;
    @JsonProperty
    private String location;
    @JsonProperty
    private String orig_prof_img_url;
    @JsonProperty
    private String prof_img_url_400;
    @JsonProperty
    private String prof_big_img_url;
    @JsonProperty
    private Integer listed_count;
    @JsonProperty
    private String prof_mini_img_url;
    @JsonProperty
    private String desc;
    @JsonProperty
    private String prf_back_img_url;
    @JsonProperty
    private String prof_img_url;
    @JsonProperty
    private String screen_name;
    @JsonProperty
    private Integer status_count;
    @JsonProperty
    private String url;
    @JsonProperty
    private Long last_status_id;
    @JsonProperty
    private String last_status_text;
    @JsonProperty
    private Integer last_status_retweet_count;
    @JsonProperty
    private Integer last_status_fav_count;
    @JsonProperty
    private String last_status_quoted_text;
    @JsonProperty
    private Long last_status_quoted_id;
    @JsonProperty
    private String last_status_quoted_screen_name;
    @JsonProperty
    private String last_status_retweeted_text;
    @JsonProperty
    private String last_status_retweeted_screen_name;
    @JsonProperty
    private Long last_status_retweeted_id;
    @JsonProperty
    private Long inserted_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public Long getCreation_datetime() {
        return creation_datetime;
    }

    public void setCreation_datetime(Long creation_datetime) {
        this.creation_datetime = creation_datetime;
    }

    public Integer getFriends_count() {
        return friends_count;
    }

    public void setFriends_count(Integer friends_count) {
        this.friends_count = friends_count;
    }

    public Integer getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(Integer follower_count) {
        this.follower_count = follower_count;
    }

    public Integer getFav_count() {
        return fav_count;
    }

    public void setFav_count(Integer fav_count) {
        this.fav_count = fav_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrig_prof_img_url() {
        return orig_prof_img_url;
    }

    public void setOrig_prof_img_url(String orig_prof_img_url) {
        this.orig_prof_img_url = orig_prof_img_url;
    }

    public String getProf_img_url_400() {
        return prof_img_url_400;
    }

    public void setProf_img_url_400(String prof_img_url_400) {
        this.prof_img_url_400 = prof_img_url_400;
    }

    public String getProf_big_img_url() {
        return prof_big_img_url;
    }

    public void setProf_big_img_url(String prof_big_img_url) {
        this.prof_big_img_url = prof_big_img_url;
    }

    public Integer getListed_count() {
        return listed_count;
    }

    public void setListed_count(Integer listed_count) {
        this.listed_count = listed_count;
    }

    public String getProf_mini_img_url() {
        return prof_mini_img_url;
    }

    public void setProf_mini_img_url(String prof_mini_img_url) {
        this.prof_mini_img_url = prof_mini_img_url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrf_back_img_url() {
        return prf_back_img_url;
    }

    public void setPrf_back_img_url(String prf_back_img_url) {
        this.prf_back_img_url = prf_back_img_url;
    }

    public String getProf_img_url() {
        return prof_img_url;
    }

    public void setProf_img_url(String prof_img_url) {
        this.prof_img_url = prof_img_url;
    }

    public String getScreen_name() {
        return screen_name;
    }

    public void setScreen_name(String screen_name) {
        this.screen_name = screen_name;
    }

    public Integer getStatus_count() {
        return status_count;
    }

    public void setStatus_count(Integer status_count) {
        this.status_count = status_count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getLast_status_id() {
        return last_status_id;
    }

    public void setLast_status_id(Long last_status_id) {
        this.last_status_id = last_status_id;
    }

    public String getLast_status_text() {
        return last_status_text;
    }

    public void setLast_status_text(String last_status_text) {
        this.last_status_text = last_status_text;
    }

    public Integer getLast_status_retweet_count() {
        return last_status_retweet_count;
    }

    public void setLast_status_retweet_count(Integer last_status_retweet_count) {
        this.last_status_retweet_count = last_status_retweet_count;
    }

    public Integer getLast_status_fav_count() {
        return last_status_fav_count;
    }

    public void setLast_status_fav_count(Integer last_status_fav_count) {
        this.last_status_fav_count = last_status_fav_count;
    }

    public String getLast_status_quoted_text() {
        return last_status_quoted_text;
    }

    public void setLast_status_quoted_text(String last_status_quoted_text) {
        this.last_status_quoted_text = last_status_quoted_text;
    }

    public Long getLast_status_quoted_id() {
        return last_status_quoted_id;
    }

    public void setLast_status_quoted_id(Long last_status_quoted_id) {
        this.last_status_quoted_id = last_status_quoted_id;
    }

    public String getLast_status_quoted_screen_name() {
        return last_status_quoted_screen_name;
    }

    public void setLast_status_quoted_screen_name(String last_status_quoted_screen_name) {
        this.last_status_quoted_screen_name = last_status_quoted_screen_name;
    }

    public String getLast_status_retweeted_text() {
        return last_status_retweeted_text;
    }

    public void setLast_status_retweeted_text(String last_status_retweeted_text) {
        this.last_status_retweeted_text = last_status_retweeted_text;
    }

    public String getLast_status_retweeted_screen_name() {
        return last_status_retweeted_screen_name;
    }

    public void setLast_status_retweeted_screen_name(String last_status_retweeted_screen_name) {
        this.last_status_retweeted_screen_name = last_status_retweeted_screen_name;
    }

    public Long getLast_status_retweeted_id() {
        return last_status_retweeted_id;
    }

    public void setLast_status_retweeted_id(Long last_status_retweeted_id) {
        this.last_status_retweeted_id = last_status_retweeted_id;
    }

    public Long getInserted_date() {
        return inserted_date;
    }

    public void setInserted_date(Long inserted_date) {
        this.inserted_date = inserted_date;
    }
}
