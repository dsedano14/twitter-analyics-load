package com.david.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class keywordConfig implements Serializable{
    @JsonProperty
    private String project;
    @JsonProperty
    private String bigqueryProject;
    @JsonProperty
    private String bigqueryDataset;
    @JsonProperty
    private List<KeyWord> keywords;

    public String getProject() {
        return project;
    }


    public String getBigqueryProject() {
        return bigqueryProject;
    }



    public String getBigqueryDataset() {
        return bigqueryDataset;
    }


    public List<KeyWord> getKeyWord() {
        return keywords;
    }



}
