package com.david.utils;


public class FillNames {

    public final static String PRO_TABLE = "raw.status_twitter";
    public final static String PRO_TABLE_HASHTAG = "raw.topic_twitter";
    public final static String PRO_TABLE_TREND = "raw.trend_twitter";
    public final static String PRO_TABLE_USER = "raw.user_twitter";
    public final static String DEVELOP_TABLE = "raw.write_test";
    public final static String LOCATION_FIELD = "location";
    public final static String FOLLOWERCOUNT_FIELD = "num_follower";
    public final static String PROFILEIMG_FIELD = "profileimg_url";
    public final static String TEXT_FIELD = "text_value";
    public final static String INGESTION_FIELD = "date_added";
    public final static String APPNAME = "TWITTER_APP";
    public final static String JOBNAME = "-twitter-app-ingestion-batch";
    public final static String JOBNAME_STREAM = "-twitter-app-ingestion-stream";
   
	/*enter your project info*/
    public final static String PROJECT = "xxxxxxxxxxxxxxxxxxxx";
	 public final static String STAGINGLOCATION ="xxxxxxxxxxxxxxxxxxxxxxxx";
    public final static String GCPTEMPLOCATION ="xxxxxxxxxxxxxxxxxxxxx";
    public final static String TEMPLOCATION ="xxxxxxxxxxxxxxxxxxxxxxxx";
    public final static String NETWORK = "global-vpc";
    public final static String SUBNETWORK = "regions/europe-west1/subnetworks/default";
    public final static String REGION = "europe-west1";
    public final static String ZONE = "europe-west1-b";
    //public final static String COUNTRY_YAML = "exchangerate-entity.yaml";
    //public final static String STRG_BUCKET = "privalia-exchangerate-temp";
	/*enter your secret keys*/
    public final static String _consumerKey="xxxxxxxxxxxxxxxxxxxxxxxx";
    public final static String _consumerSecret="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    public final static String _accessToken="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
    public final static String _ACCESSTOKENSECRET="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

    //TWEETER STREAMS
    public final static String STATUS_CREATED_AT="status_creation_datetime";
    public final static String STATUS_ID="status_id";
    public final static String STATUS_TEXT="status_text_value";
    public final static String REPLIED_STATUS_ID="status_replied_id";
    public final static String REPLIED_USER_ID="user_replied_id";
    public final static String ISFAVORITE="is_favorite";
    public final static String ISRETEWEETED="is_retweeted";
    public final static String FAVORITE_COUNT="favorite_count";
    public final static String REPLIED_SCREEN_NAME="replied_screen_name";
    public final static String STATUS_GEOLOCATION_LATITUDE="status_geolocation_latitude";
    public final static String STATUS_GEOLOCATION_LONGITUDE="status_geolocation_longitude";
    public final static String STATUS_PLACE_COUNTRY="status_place";
    public final static String STATUS_PLACE_FULLNAME="status_place_fullname";
    public final static String STATUS_PLACE_NAME="status_place_name";
    public final static String STATUS_PLACE_TYPE="status_place_type";
    public final static String STATUS_PLACE_ADDRESS="status_place_address";
    public final static String STATUS_PLACE_URL="status_place_url";
    public final static String IS_SENSITIVE="is_sensitive";
    public final static String STATUS_LANG="status_language";
    public final static String STATUS_CONTRIBUTORS="contributors";
    public final static String RETWEETED_STATUS_TEXT="status_retweeted_text";
    public final static String RETWEETED_STATUS_USER_NAME="status_retweeted_user_name";
    public final static String RETWEETED_STATUS_USER_IMG="status_retweeted_user_img";
    public final static String RETWEETED_STATUS_ID="status_retweeted_status_id";
    public final static String RETWEETED_STATUS_USER_SCREEN_NAME="status_retweeted_user_screen_name";
    public final static String HASHTAG_ENTITY="hashtag_entity";
    public final static String USER_ID="user_id";
    public final static String USER_NAME="user_name";
    public final static String USER_EMAIL="user_email";
    public final static String USER_SCREENNAME="user_screenname";
    public final static String USER_LOCATION="user_location";
    public final static String USER_DESC="user_desc";
    public final static String USER_PROFILE_IMG_URL="user_profile_img_url";
    public final static String USER_PROFILE_IMG_URL_HTTPS="user_profile_img_url_https";
    public final static String USER_URL="user_url";
    public final static String USER_FOLLOWER_COUNT="user_follower_count";
    public final static String USER_STATUS="user_status";
    public final static String USER_PROF_BACK_COLOR="user_prof_background_color";
    public final static String USER_PROF_TEXT_COLOR="user_prof_text_color";
    public final static String USER_PROF_LINK_COLOR="user_prof_link_color";
    public final static String USER_SIDEBAR_COLOR="user_sidebar_color";
    public final static String USER_SIDEBAR_BORDER_COLOR="user_side_border_color";
    public final static String USER_FRIENDS_COUNT="user_friends_count";
    public final static String USER_CREATION_DATETIME="user_creation_datetime";
    public final static String USER_FAV_COUNT="user_favorite_count";
    public final static String USER_LANG="user_lang";
    public final static String USER_STATUS_COUNT="user_status_count";
    public final static String USER_IS_VERIFIED="user_is_verified";
    public final static String USER_PROF_BANNER_300x100="user_prof_banner_300_100";
    public final static String USER_PROF_BANNER_600x200="user_prof_banner_600_200";
    public final static String USER_PROF_IMG_400x400="user_prof_img_400_400";
    public final static String STATUS_QUOTED="status_quoted";
    public final static String STATUS_QUOTED_ID="status_quoted_id";
    public final static String STATUS_QUOTED_SCREEN_NAME="status_quoted_screen_name";

//TweeterTrends
    public final static String TREND_NAME="trend_name";
    public final static String TREND_QUERY="trend_query";
    public final static String TREND_TWEETVOLUME="trend_tweet_volume";
    public final static String TREND_URL="trend_url";
    public final static String TREND_PLACE="trend_place";

    //TweeterUsers
    public final static String USER_ORIG_PROFILE_IMG_URL="user_orig_profile_img_url";
    public final static String USER_BIG_PROFILE_IMG_URL="user_big_profile_img_url";
    public final static String USER_LISTED_COUNT="user_listed_count";
    public final static String USER_MINI_PROFILE_IMG_URL="user_mini_profile_img_url";
    public final static String USER_PROF_BACK_IMG_URL="user_profile_back_img_url";
    public final static String STATUS_RETWEET_COUNT="status_retweet_count";
    public final static String STATUS_FAV_COUNT="status_fav_count";

}
