package com.david.utils;
import com.david.model.*;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.runners.dataflow.options.DataflowPipelineWorkerPoolOptions;
import org.apache.beam.sdk.options.Description;
import org.apache.beam.sdk.transforms.DoFn;
import org.slf4j.LoggerFactory;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import java.time.Instant;
import java.util.*;

import static com.david.utils.FillNames.*;
import static java.lang.Math.abs;


public class Utils {
    private static org.slf4j.Logger LOGGER = LoggerFactory.getLogger(Utils.class);


    public static CustomOptions setOptions(CustomOptions options, String environment, String type){
        options.setAppName(environment+FillNames.APPNAME);
        options.setJobName(type.equals("batch") ? environment+FillNames.JOBNAME : environment+ JOBNAME_STREAM);
        options.setStagingLocation(FillNames.STAGINGLOCATION);
        options.setProject(FillNames.PROJECT);
        options.setNetwork(FillNames.NETWORK);
        options.setSubnetwork(FillNames.SUBNETWORK);
        options.setRegion(FillNames.REGION);
        options.setZone(FillNames.ZONE);
        options.setTempLocation(FillNames.TEMPLOCATION);
        //options.setTempLocation("gs://app-twitter-temp/temp-loc");
        options.setGcpTempLocation(FillNames.GCPTEMPLOCATION);
        options.setAutoscalingAlgorithm(DataflowPipelineWorkerPoolOptions.AutoscalingAlgorithmType.THROUGHPUT_BASED);
        options.setMaxNumWorkers(1);
        options.setRunner(DataflowRunner.class);

        return options;
    }

    public interface CustomOptions extends DataflowPipelineOptions {
        @Description("Custom options")
        String getEnvironment();
        void setEnvironment(String environment);
    }

    public static TableSchema getValidationSchema() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName(FillNames.LOCATION_FIELD).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.TEXT_FIELD).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.PROFILEIMG_FIELD).setType("STRING"));
        fields.add(new TableFieldSchema().setName(FillNames.FOLLOWERCOUNT_FIELD).setType("INTEGER"));
        TableSchema schema = new TableSchema().setFields(fields);

        return schema;
    }


    public static Long setUTCToUnixTimestamp(Date o){

        Long time = o.getTime()/1000;
        return time;

    }


    public static boolean checkIfExistsQuoted(Status status){
        Boolean exist=null;
        Long obj1 = new Long(1);
        Long quotedStatusId =abs(status.getQuotedStatusId());
        /*System.out.println(abs(status.getQuotedStatusId()));
        System.out.println(quotedStatusId);
        System.out.println(quotedStatusId.equals(obj1));
*/

        if(!quotedStatusId.equals(obj1)) {
            exist = true;
        }else{
            exist=false;
        }

        return exist;
    }

    public static Long getCurrentTimestamp(){
        Instant instant = Instant.now();
        long timeStampSeconds = instant.getEpochSecond();
        return timeStampSeconds;
    }



    public static TableSchema getValidationSchemaTrends() {
        List<TableFieldSchema> fields = new ArrayList<>();
        fields.add(new TableFieldSchema().setName(TREND_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(TREND_QUERY).setType("STRING"));
        fields.add(new TableFieldSchema().setName(TREND_TWEETVOLUME).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(TREND_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(TREND_PLACE).setType("STRING"));
        fields.add(new TableFieldSchema().setName("inserted_date").setType("TIMESTAMP"));
        TableSchema schema = new TableSchema().setFields(fields);

        return schema;
    }

    public static class processTweeterTrends extends DoFn<TweetTrend, TableRow> {

        @ProcessElement
        public void processElement(ProcessContext c) {


            try{
                System.out.println("--------------process---------------");

                TableRow row = new TableRow();
                row.set(TREND_NAME, c.element().getName());
                row.set(TREND_QUERY, c.element().getQuery());
                row.set(TREND_TWEETVOLUME, c.element().getTweetVolume());
                row.set(TREND_URL, c.element().getUrl());
                row.set(TREND_PLACE, c.element().getPlace());
                row.set("inserted_date",getCurrentTimestamp());

                c.output(row);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static TableSchema getValidationSchemaUsers() {

        List<TableFieldSchema> fields = new ArrayList<>();

        fields.add(new TableFieldSchema().setName(USER_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_CREATION_DATETIME).setType("TIMESTAMP"));
        fields.add(new TableFieldSchema().setName(USER_FRIENDS_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_FOLLOWER_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_FAV_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_LANG).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_EMAIL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_IS_VERIFIED).setType("BOOLEAN"));
        fields.add(new TableFieldSchema().setName(USER_LOCATION).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_ORIG_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROF_IMG_400x400).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROF_BANNER_300x100).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROF_BANNER_600x200).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_BIG_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_LISTED_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_MINI_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_DESC).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROF_BACK_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROFILE_IMG_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_PROFILE_IMG_URL_HTTPS).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_SCREENNAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_STATUS_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(USER_PROF_BACK_COLOR).setType("STRING"));
        fields.add(new TableFieldSchema().setName(USER_URL).setType("STRING"));
        fields.add(new TableFieldSchema().setName(STATUS_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_CREATED_AT).setType("TIMESTAMP"));
        fields.add(new TableFieldSchema().setName(STATUS_TEXT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(STATUS_RETWEET_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_FAV_COUNT).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_LANG).setType("STRING"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED).setType("STRING"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName(STATUS_QUOTED_SCREEN_NAME).setType("STRING"));
        fields.add(new TableFieldSchema().setName(RETWEETED_STATUS_TEXT).setType("STRING"));
        fields.add(new TableFieldSchema().setName(RETWEETED_STATUS_ID).setType("INTEGER"));
        fields.add(new TableFieldSchema().setName("inserted_date").setType("TIMESTAMP"));
        TableSchema schema = new TableSchema().setFields(fields);

        return schema;
    }

    public static class processTweeterUsers extends DoFn<User, TableRow> {

        @ProcessElement
        public void processElement(ProcessContext c) {



            try{
                System.out.println("--------------process users---------------");

                TableRow row = new TableRow();
                row.set(USER_ID, c.element().getId());
                row.set(USER_CREATION_DATETIME,setUTCToUnixTimestamp(c.element().getCreatedAt()));
                row.set(USER_FRIENDS_COUNT, c.element().getFriendsCount());
                row.set(USER_FOLLOWER_COUNT, c.element().getFollowersCount());
                row.set(USER_FAV_COUNT, c.element().getFavouritesCount());
                row.set(USER_NAME, c.element().getName());
                row.set(USER_LANG, c.element().getLang());
                row.set(USER_EMAIL, c.element().getEmail());
                row.set(USER_IS_VERIFIED, c.element().isVerified());
                row.set(USER_LOCATION, c.element().getLocation());
                row.set(USER_ORIG_PROFILE_IMG_URL, c.element().getOriginalProfileImageURL());
                row.set(USER_PROF_IMG_400x400, c.element().get400x400ProfileImageURL());
                row.set(USER_PROF_BANNER_300x100,c.element().getProfileBanner300x100URL());
                row.set(USER_PROF_BANNER_600x200,c.element().getProfileBanner600x200URL());
                row.set(USER_BIG_PROFILE_IMG_URL, c.element().getBiggerProfileImageURL());
                row.set(USER_LISTED_COUNT, c.element().getListedCount());
                row.set(USER_MINI_PROFILE_IMG_URL, c.element().getMiniProfileImageURL());
                row.set(USER_DESC, c.element().getDescription());
                row.set(USER_PROF_BACK_IMG_URL, c.element().getProfileBackgroundImageURL());
                row.set(USER_PROFILE_IMG_URL, c.element().getProfileImageURL());
                row.set(USER_PROFILE_IMG_URL_HTTPS, c.element().getProfileImageURLHttps());
                row.set(USER_SCREENNAME, c.element().getScreenName());
                row.set(USER_STATUS_COUNT, c.element().getStatusesCount());
                row.set(USER_PROF_BACK_COLOR, c.element().getProfileBackgroundColor());
                row.set(USER_URL, c.element().getURL());
                row.set(STATUS_ID, c.element().getStatus().getId());
                row.set(STATUS_CREATED_AT, setUTCToUnixTimestamp( c.element().getStatus().getCreatedAt()));
                row.set(STATUS_TEXT, c.element().getStatus().getText());
                row.set(STATUS_RETWEET_COUNT, c.element().getStatus().getRetweetCount());
                String fav[] = c.element().toString().split("favoriteCount=");
                String fav1[] = fav[1].split(",");
                row.set(STATUS_FAV_COUNT, Integer.parseInt(fav1[0]));
                row.set(STATUS_LANG, c.element().getStatus().getLang());

                if(checkIfExistsQuoted(c.element().getStatus())) {
                    try{
                        row.set(STATUS_QUOTED, c.element().getStatus().getQuotedStatus().getText());
                        row.set(STATUS_QUOTED_ID, c.element().getStatus().getQuotedStatus().getId());
                        row.set(STATUS_QUOTED_SCREEN_NAME, c.element().getStatus().getQuotedStatus().getUser().getScreenName());
                    }catch(Exception e){
                        row.set(STATUS_QUOTED, "Not Quoted");
                        row.set(STATUS_QUOTED_ID, 0);
                        row.set(STATUS_QUOTED_SCREEN_NAME, "Not Quoted");
                    }


                }else{
                    row.set(STATUS_QUOTED, "Not Quoted");
                    row.set(STATUS_QUOTED_ID, 0);
                    row.set(STATUS_QUOTED_SCREEN_NAME, "Not Quoted");
                 }

                if(c.element().getStatus().isRetweet()){
                    row.set(FillNames.RETWEETED_STATUS_TEXT,c.element().getStatus().getRetweetedStatus().getText());
                    row.set(FillNames.RETWEETED_STATUS_ID,c.element().getStatus().getRetweetedStatus().getId());
                }else{
                row.set(FillNames.RETWEETED_STATUS_TEXT,"Not Retweeted");
                row.set(RETWEETED_STATUS_ID,0);
                }

                row.set("inserted_date",getCurrentTimestamp());

                c.output(row);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }



    public static ConfigurationBuilder SetOptionsTwitter(){
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true);
        cb.setOAuthConsumerKey(_consumerKey);
        cb.setOAuthConsumerSecret(_consumerSecret);
        cb.setOAuthAccessToken(_accessToken);
        cb.setOAuthAccessTokenSecret(_ACCESSTOKENSECRET);
        cb.setJSONStoreEnabled(true);
        cb.setIncludeEntitiesEnabled(true);

        return cb;
    }
}
